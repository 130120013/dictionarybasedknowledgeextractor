# SystemAssistant.py
from re import T
import NLUtils
import KnowledgeExtractor
import GPTExtractor


import sys
import os
import PdfConverter

def detect_theory(text):
    statistic_gpt = [["statistical", 0], ["geometric", 0], ["wave", 0], ["qualitative", 0], ["hybrid", 0]]
    stemmed_text = NLUtils.stem(text)
    
    for theory in statistic_gpt:
        matched_theory = NLUtils.stem(theory[0])[0] in stemmed_text
        if matched_theory:
            KnowledgeExtractor.inc_theory_count(statistic_gpt, theory[0])
    
    theory = ""
    for t in statistic_gpt:
        if t[1] > 0:
            if theory == "":
                theory = t[0]
            else:
                break    

    return [statistic_gpt, theory]

def resolve_cluster():
    # Count occurrences of the words "wave", "geometric", "statistical"
    # corpus_statistic = KnowledgeExtractor.get_theory_statistic()
    
    GptResponce = GPTExtractor.get_response("", GPTExtractor.get_token())
    # print(GptResponce['choices'][0]['message']['content'])
    text = GptResponce['choices'][0]['message']['content']
    gpt_statistic = detect_theory(text)
    print(gpt_statistic[1])
    corpusStatistic = KnowledgeExtractor.get_theory_statistic()
    print(corpusStatistic)
    # If both subsystems would return the same theory, so return this. 
    # If it is not, show F1-score of every subsystem and ask user to clarify.
    # Label data for dictionary/LLM


filepath = sys.argv[1]
if os.path.exists(filepath):
    PdfConverter.pdf_to_txt(filepath)
for root, _, files in os.walk(filepath):
    for file in files:
            if file.endswith('.txt'):
                file_path_txt = os.path.join(root, file)
                res = KnowledgeExtractor.extract_data(file_path_txt)
                print(res)
                print('\n')
                
resolve_cluster()   
