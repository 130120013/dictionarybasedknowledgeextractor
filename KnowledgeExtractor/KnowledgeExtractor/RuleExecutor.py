import re


def filter_nodes_by_tag(tagname, tree):
    return [t for t in tree if t[0].label() == tagname]

def get_trigger_lambdas(trigger_name, trigger_words):
    rules = [t[1] for t in trigger_words if t[0] == trigger_name]
    return rules

def get_trigger_lambda_args(trigger_name, trigger_words):
    args = [t[2] for t in trigger_words if t[0] == trigger_name]
    return args

def rule_exec(trees, trigger_lambdas, trigger_lambda_args):
    i = 0
    temp = []
    for rule in trigger_lambdas:
        if trees[0].height() > 1:
            rule_exec_res = list(filter(lambda x: len(x[0].leaves()) == 1 or rule(x, trigger_lambda_args[i]), trees))
            temp.extend(rule_exec_res)
            i = i + 1
    # filter list and return unique values
    temp = [x for i, x in enumerate(temp) if i == temp.index(x)]
    return temp           

def all_rules_exec(trees, trigger_words):
    i = 0
    temp = []
    for tw in trigger_words:
        if trees[0].height() > 1:
            rule_exec_res = list(filter(lambda x: len(x[0].leaves()) == 1 and 
                                        re.match(tw[2], '', ), trees)) # one node entity processing
            rule_exec_res.extend(list(filter(
                lambda x: len(x[0].leaves()) != 1 and tw[1](x, tw[2]), trees))
                                    )
            
            if len(rule_exec_res) > 0:
                temp.append((tw[0], rule_exec_res))
            i = i + 1
    # filter list and return unique values
    temp = [x for i, x in enumerate(temp) if i == temp.index(x)]
    return temp           