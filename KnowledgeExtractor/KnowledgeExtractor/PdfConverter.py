import fitz
import os
def pdf_to_txt(folder_path):
    def extract_text_from_pdf(pdf_path):
        text = ''
        document = fitz.open(pdf_path)
        for page_num in range(document.page_count):
            page = document[page_num]
            text += page.get_text()
        return text

    def save_text_to_file(text, file_path):
        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(text)

    def convert_pdf_to_txt(pdf_path, txt_path):
        text = extract_text_from_pdf(pdf_path)
        save_text_to_file(text, txt_path)

    def process_folder(folder):
        folder_idx = 0
        for root, _, files in os.walk(folder):
            file_idx = 0
            for file in files:
                if file.endswith('.pdf'):
                    pdf_path = os.path.join(root, file)
                    txt_name = f"parsed{folder_idx}-{file_idx}.txt"
                    txt_path = os.path.join(root, txt_name)
                    convert_pdf_to_txt(pdf_path, txt_path)
                    file_idx = file_idx + 1        
            folder_idx = folder_idx + 1

    process_folder(folder_path)
