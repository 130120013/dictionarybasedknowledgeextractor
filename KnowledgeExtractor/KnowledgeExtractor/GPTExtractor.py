from pickle import FALSE
import requests
import uuid
import json

def get_token():
    url = "https://ngw.devices.sberbank.ru:9443/api/v2/oauth"

    payload='scope=GIGACHAT_API_PERS'
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'RqUID': str(uuid.uuid4()),
      'Authorization': 'Basic ZDNkMDY2MTMtNjE3ZS00ZjhiLWEzM2UtZjg2NmM1NzhhYjUxOjFlMzYzZjQ0LTVmODUtNDY4OS1iYTQ1LTNiNWI5OWZmNWZiZg=='
    }

    #response = requests.request("POST", url, headers=headers, data=payload, verify='C:\\python\\39\\lib\\site-packages\\certifi\\russian_trusted_root_ca.cer')
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)

    #print(response.text)
    return response.json()["access_token"]


def get_response(query, token):
    url = "https://gigachat.devices.sberbank.ru/api/v1/chat/completions"

    auth = 'Bearer ' + token
    payload = json.dumps({
      "model": "GigaChat:latest",
      "messages": [
        {
          "role": "user",
          "content": "What acoustic theory - statistical, geometric, wave - does the described method belong to, explain decision, mark it with \"EXPL\": \" Different methods for simulation of sound propagation have different advantages and disadvantages regarding application in dispersive, non-homogenous \
media. There is not a single method that is able to fulfill all demands of such an \
environment. The methods that use principals of geometrical acoustics do not take \
refraction into account, and FEM can not be efficient enough in environments with \
complex 3D boundaries. \
In order to solve this problem a hybrid algorithm for beam-tracing method is designed. \
This algorithm is based on stochastical method, and is intended to solve the problem of the refraction, without sacrificing the accuracy and efficiency of beam-tracing method. \" "
        }
      ],
      "temperature": 1,
      "top_p": 0.1,
      "n": 1,
      "stream": False,
      "max_tokens": 5000,
      "repetition_penalty": 1
    })
    headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': auth
    }

    #response = requests.request("POST", url, headers=headers, data=payload, verify='C:\\python\\39\\lib\\site-packages\\certifi\\russian_trusted_root_ca.cer')
    response = requests.request("POST", url, headers=headers, data=payload, verify=False)
    # headers = {"Authorization": f"Bearer {token}"}
    # response = requests.get(f"https://api.sber.ai/api/dialogic/v1/chat/generate?text={query}", headers=headers, verify=False)
    return response.json()

#token = get_token()
#response = get_response("Hello, how are you?", )
#print(response)
