from nltk.stem.snowball import SnowballStemmer

def stem(tokens):
    snow_stemmer = SnowballStemmer(language='english')
    stem_words = []
    for w in tokens:
        x = snow_stemmer.stem(w)
        stem_words.append(x)

    return stem_words