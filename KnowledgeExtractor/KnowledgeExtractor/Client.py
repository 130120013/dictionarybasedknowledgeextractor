import sys
import os
import KnowledgeExtractor
import PdfConverter

filepath = sys.argv[1]
if os.path.exists(filepath):
    PdfConverter.pdf_to_txt(filepath)
for root, _, files in os.walk(filepath):
    for file in files:
            if file.endswith('.txt'):
                file_path_txt = os.path.join(root, file)
                KnowledgeExtractor.extract_data(file_path_txt)

# KnowledgeExtractor.extract_data("test")