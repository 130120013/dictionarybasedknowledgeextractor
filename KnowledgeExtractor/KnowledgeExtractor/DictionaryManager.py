import json
import re
from nltk.tokenize import word_tokenize, sent_tokenize
from numpy import printoptions
from NLUtils import stem

# process rule ^. Example: "$ ^measurements" -> ["$ m", "$ cm"]
def expand_rules(trigger_words):
    for item in trigger_words:
        unprocessed_rules = [w for w in item[1] if '^' in w]
        for w in unprocessed_rules:
            new_rules = []
            idx = w.index('^')
            temp_w = w[idx+1:]
            before_w = w[:idx-1]
            temp_w = re.sub(r'[^\w+_{0,}]', '', temp_w)
            rules = filter(lambda x: x[0] == temp_w, trigger_words)
            for r in rules:
                for n in r[1]:
                    new_rules.append(before_w + " " + n)
            item[1].remove(w)
            item[1].extend(new_rules)
    return trigger_words

# $ - placeholder for number, ^ - use rule
def process_trigger_words(trigger_words):
    trigger_words = expand_rules(trigger_words)
    processed_words = []
    for item in trigger_words:
        for w in item[1]:
            has_placeholder = '$' in w
            if has_placeholder:
                placeholder_idx = w.index('$')
                mark = w[:placeholder_idx] if len(w) - 1 == placeholder_idx else w[placeholder_idx:]
                mark = re.sub(r'[^\w+]', '', mark)
                s = w.split(' ')
                token_pos = s.index(mark) if mark != '' else 0
                # idk how to capture value in Python like C++ so I have to use carring technique
                processed_words.append((item[0], lambda x, m, p=token_pos: (re.sub(r'[^\w]', '', 
                                                                                   x[0][p if len(x[0].leaves()) - 1 >= p else 0 ][0]) == m), mark.lower()))
    return processed_words

def get_trigger_rules():
    json_file = open('domain-dictionary.json', mode="r")
    data = json.load(json_file)
    trigger_words = []
    for item in data:
        trigger_words.append((item, data[item]))
    processed_rules = process_trigger_words(trigger_words)
    json_file.close()
    return processed_rules

def get_domain_words():
    json_file = open('domain-dictionary.json', mode="r")
    data = json.load(json_file)
    specific_words = []
        
    for item in data:
        if item == 'domain_words':
            specific_words.extend(data[item])
   
    json_file.close()
    return specific_words

def get_theory_words():
    json_file = open('domain-dictionary.json', mode="r")
    data = json.load(json_file)
    specific_words = []
        
    for item in data:
        if item == 'theory_words':
            words = data[item]
            for w in words:
                p = (w, words[w])
                specific_words.append(p)
   
    json_file.close()
    return specific_words

def highlight_domain_words(document):
    domain_words = get_domain_words()
    sentences = sent_tokenize(document)
    processed_sentences = []
    
    stemmed_domain_words = stem(domain_words)

    for sen in sentences:
        w = word_tokenize(sen)
        stemmed_w = stem(w)
        for dw in stemmed_domain_words:
            if dw in stemmed_w:
                dw_id = stemmed_w.index(dw)
                found_dw_idx = sen.index(w[dw_id])
                new_text = sen[:found_dw_idx] + ' DOMAIN! ' + sen[found_dw_idx:]
                sen = new_text
        processed_sentences.append(sen)

    processed_text = " ".join(processed_sentences)
    return processed_text

def highlight_theory_words(document):
    theory_words = get_theory_words()
    sentences = sent_tokenize(document)
    processed_sentences = []
    found_words = []
    only_words = []
    found_theories = []
    for w in theory_words:
        only_words.extend(w[1])
    
    stemmed_theory_words = stem(only_words)

    for sen in sentences:
        w = word_tokenize(sen)
        stemmed_w = stem(w)
        for tw in stemmed_theory_words:
            if tw in stemmed_w:
                tw_id = stemmed_w.index(tw)
                found_tw_idx = sen.index(w[tw_id])
                new_text = sen[:found_tw_idx] + ' DOMAIN! ' + sen[found_tw_idx:]
                sen = new_text
                found_words.append(tw)
        processed_sentences.append(sen)

    for w in theory_words:
        for fs in found_words:
            if fs in stem(w[1]):
                found_theories.append((w[0], fs))

    processed_text = " ".join(processed_sentences)
    return processed_text, found_theories

def get_filtration_words():
    json_file = open('common-dictionary.json', mode="r")
    data = json.load(json_file)
    trigger_words = []
    for item in data:
        trigger_words.append((item, data[item]))
    json_file.close()
    return trigger_words

def filter_sentences(document):
    trigger_words = []
    sentences_with_word = []

    for t in get_filtration_words():
        trigger_words.extend(t[1])
        
    sentences = sent_tokenize(document)
    stemmed_sent = []
    trigger_words = stem(trigger_words)

    for sen in sentences:
        w = word_tokenize(sen)
        stemmed_w = stem(w)
        if len(set(stemmed_w).intersection(trigger_words)) > 0:
            sentences_with_word.append(sen)

    return sentences_with_word


