See discussions, stats, and author profiles for this publication at: https://www.researchgate.net/publication/324063841
Acoustic Comfort Evaluation with the Simulation Program Specic to the
Educational Buildings of Bozok University Classrooms
Chapter  March 2018
DOI: 10.1007/978-3-319-63709-9_13
CITATIONS
0
READS
220
4 authors:
Zuhal zetin
Usak niversitesi
20 PUBLICATIONS18 CITATIONS
SEE PROFILE
Fusun Demirel
Gazi University
36 PUBLICATIONS52 CITATIONS
SEE PROFILE
Merve Grkem
Gazi University
7 PUBLICATIONS3 CITATIONS
SEE PROFILE
Gl lisulu
Baskent University
4 PUBLICATIONS2 CITATIONS
SEE PROFILE
All content following this page was uploaded by Zuhal zetin on 27 July 2018.
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
 
 
ACOUSTIC COMFORT EVALUATION WITH THE SIMULATION PROGRAM            
SPECIFIC TO THE EDUCATIONAL BUILDINGS OF BOZOK UNIVERSITY 
CLASSROOMS 
 
 
 
 
 
Research Assistant  Zuhal zetina , Prof. Dr.  Fsun Demirelb, PhD Student Merve Grkemc *, Lecturer 
- S. Gl Ilisulud 
 
a Bozok University, Faculty of Engineering & Architecture, Department of Architecture, Yozgat, Turkey, 
zuhal.ozcetin@bozok.edu.tr  
b Gazi University, Faculty of Architecture, Department of Architecture, Ankara, Turkey, 
fusundgk@gmail.com 
 
c * Gazi University, Faculty of Architecture, Department of Architecture, Ankara, Turkey, 
mervedogangorkem@gmail.com 
d Bakent University, Faculty of Fine Arts, Design and Architecture, Ankara, Turkey, islekg@baskent.edu.tr 
 
 
Abstract 
 
Spaces that possess the acoustic comfort, which people 
require, can be provided by building structures that are 
suitable for work and living by solving acoustic problems, 
which will influence the performance of work by affecting 
the health and the comfort of the peoples in a negative 
way.
Within this context, in this paper as of specific 
to educational buildings, an acoustic analysis study is 
conducted through Odeon (v 10.02) simulation program 
aimed for the acoustic comfort conditions of the 
classrooms located in the Faculty of Engineering & 
Architecture of Bozok University.
Keywords: Educational buildings, classrooms, acoustic 
comfort, Odeon.
These standards have brought up the 
imperativeness of the design by taking into consideration 
of the factors, which fulfill its intended purpose and 
appropriate acoustic conditions of all the rooms and other 
spaces found in the buildings for educational purposes.
With regard to solving the education problem of our 
country, decreasing the quality and the quantity of the 
educationalists as well as the importance of building 
decent schools should not be overlooked.
The Acoustic Comfort Conditions in the 
Educational Buildings 
 
It is an undeniable fact that the comprehension of a subject 
which is verbalized in the classrooms is directly 
proportional with the intelligibility of speech of the 
educator.
zetin, Zuhal - Demirel, Fsun - Grkem, Merve and Ilisulu S.Gl 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
3.
The sound absorption coefficients of the materials which 
are used in the analysis study executed with the Odeon 
program are given in the Table 1.
Reverberation Time (T30) 
 
Reverberation time, which is defined as the time it takes 
the sound pressure level to diminish or decay to 60 dB 
after the sound source has been shut down in a closed 
space, is one of the most important parameters of the 
room acoustics [3,4,5].
Reverberation 
time 
values 
of 
medium 
frequencies 
recommended for the classrooms found in educational 
buildings are between 0,6 - 0,8 seconds [6,7,8,9].
T30  Reverberation time results 
T30 
(sec) 
63 
Hz 
125 
Hz 
250 
Hz 
500 
Hz 
1000 
Hz 
2000 
Hz 
4000 
Hz 
8000 
Hz 
Min 
1,16 
1,17 
1,52 
1,89 
2,55 
2,14 
1,47 
0,76 
Max 
1,20 
1,23 
1,56 
1,93 
2,61 
2,18 
1,53 
0,81 
Aver.
1,17 
1,20 
1,54 
1,90 
2,59 
2,16 
1,50 
0,79 
 
zetin, Zuhal - Demirel, Fsun - Grkem, Merve and Ilisulu S.Gl 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
 
Figure 3.
Early Decay Time (EDT) 
 
Early decay time represents the initial phase of the 
decay of the sound pressure level, which is obtained by 
multiplying by 6, the time elapsed for the decay of 10 dB 
decrease in sound pressure level after  the sound source 
has been shut down in a closed space [4,5].
The more the reverberation time increases the 
more the EDT value increases.
Nevertheless, when the analysis results are examined, it 
can be seen that the EDT values in the medium 
frequencies 
(500 
Hz-1000 
Hz) 
are equal 
to 
the 
reverberation time (Table 2-3).
EDT- Early decay time results 
EDT 
(sec) 
63 
Hz 
125 
Hz 
250 
Hz 
500 
Hz 
1000 
Hz 
2000 
Hz 
4000 
Hz 
8000 
Hz 
Min 
1,13 
1,23 
1,50 
1,87 
2,48 
2,08 
1,47 
0,76 
Max 
1,26 
1,29 
1,63 
1,98 
2,62 
2,17 
1,58 
0,88 
Aver.
Distribution diagram of EDT parameter 
zetin, Zuhal - Demirel, Fsun - Grkem, Merve and Ilisulu S.Gl 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
 
500 Hz 
 
1000 Hz 
Figure 7.
Speech Transmission Index (STI) 
 
Speech transmission index (STI) is an acoustic parameter 
developed for the measurement of the intelligibility of the 
speech based upon the speech transmission channel and 
speech transmission quality [11].
STI  Speech transmission index results 
STI 
 
Minimum 
0,47 
Maximum 
0,50 
Average 
0,48 
 
 
Figure 8.
Definiton (D50) 
 
Definition (D50) is a room acoustic parameter developed 
for the intelligibility quality of the speech performance.
In 
order the intelligibility quality of the speech performance to 
be high, it is recommended that definition parameter value 
would be higher than 0,50 in all frequencies [9,12].
D50  Definition parameter results 
D50 
63 
Hz 
125 
Hz 
250 
Hz 
500 
Hz 
1000 
Hz 
2000 
Hz 
4000 
Hz 
8000 
Hz 
Min 
0,40 
0,39 
0,31 
0,26 
0,20 
0,23 
0,32 
0,53 
Max 
0,50 
0,48 
0,40 
0,34 
0,28 
0,32 
0,42 
0,64 
Aver.
0,44 
0,42 
0,35 
0,29 
0,23 
0,27 
0,36 
0,58 
 
zetin, Zuhal - Demirel, Fsun - Grkem, Merve and Ilisulu S.Gl 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
 
Figure 10.
SPLA  A-weighted sound pressure level results 
SPLA (dB) 
 
Min 
69,7 
Max 
71,0 
Aver.
SPL  Sound pressure level parameter results 
SPL 
63 
Hz 
125 
Hz 
250 
Hz 
500 
Hz 
1000 
Hz 
2000 
Hz 
4000 
Hz 
8000 
Hz 
Min 
60,4 
60,7 
62,2 
63,2 
64,6 
63,5 
61,6 
57,9 
Max 
62,1 
62,4 
63,6 
64,5 
65,5 
64,7 
63,2 
60,4 
Aver.
Conclusion and Evaluation 
 
The acoustic comfort conditions necessary to ensure the 
speech function is analyzed through the simulation 
program Odeon which is intended for the room acoustic in 
the classrooms located in the Department of Architecture 
of Bozok University and is evaluated according to the 
regulations in force and parameters that are recommended 
in the literature in national and international standards.
According to the evaluation result, it was determined that 
the sound pressure level variation is within reasonable 
limits and that the parameters of reverberation time (T30), 
early decay time (EDT), speech transmission index (STI), 
definition (D50) are not suitable for the classroom.
According to the results of the reverberation time 
presented by the Odeon simulation program, it has been 
zetin, Zuhal - Demirel, Fsun - Grkem, Merve and Ilisulu S.Gl 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
seen that there is not a proper distribution in terms of the 
frequency in the classroom.
It was also determined that 
with the value of 2,24 seconds, the reverberation time 
which is also affecting the intelligibility of the speech 
specific to the educational buildings is not in between the 
proper values (0,6-0,8 seconds) and that the reverberation 
is high.
In compliance with this, it is 
recommended that curtains with a high absorption quality 
to be used for the windows which are located in the 
classroom and that on the back wall and on the ceiling a 
sound absorber material which will ensure the acoustic 
conditions to be utilized.
According to the result of the analysis (Table 8), it was 
decided that the parameters which are considered 
unsuitable regarding the speech transmission index (STI) 
and the definition (D50), can be decreased by reducing the 
reflective surfaces and by designing absorber surfaces in 
proper spots.
Evaluation of results 
Room 
acoustic 
parameters 
Optimum values 
 
Analysis 
results 
Evaluation 
 
 
 
 
 
T30.mid 
Reverberation 
Time, sec 
0,6  T30,mid   0,8 
(500 Hz - 1000 
Hz)   
[9] 
2,24 
NOT SUITABLE 
 
 
 
 
 
EDT 
Early Decay 
Time, sec 
EDT < T30,mid 
(500 Hz - 1000 
Hz) 
[9] 
2,24 
NOT SUITABLE 
 
 
 
 
 
SPL 
Sound 
pressure 
level, dB 
SPL < 10 dB 
[9] 
1,3 
APPROPRIATE 
 
 
 
 
 
STI 
Speech 
Transmission 
Index 
 
0,60 < Good < 0,75 
[11] 
0,48 
 
NOT SUITABLE 
 
 
 
 
 
D50 
Definition 
D50 > 0,50 
At all 
frequencies 
[9,12] 
125Hz 
0,42 
NOT 
SUITABLE 
250Hz 
0,35 
500Hz 
0,29 
1000Hz 
0,23 
2000Hz 
0,27 
4000Hz 
0,36 
 
 
 
 
 
References 
 
[1].
Building Bulletin 93, Acoustic Design of Schools; a 
Design Guide, Architects and Building Branch, 
London.
Concert Halls and Opera 
Houses Music Acoustics and Architecture, 2nd ed., 
Springer Verlag Inc., New York.
Ankara Musiki 
Muallim Mektebi Mamak Municipality Conservatory 
Building and Noise Control Analysis.
Architectural Acoustics Principles and Design Merrill 
Prentice Hall, 301-306.
Die Richtungsverteilung und 
Zeitfolge der Schallrckwrfe in Rumen - Directional 
distribution and time sequence of sound reflections in 
rooms, Acustica, 1: 31-32.
