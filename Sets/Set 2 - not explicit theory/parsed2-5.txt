Citation: Latifah, F.N.; Putri, R.S.;
Syukur, F.; Sutarno, W.H.; Paramita,
B.; Ramadhan, T. An Analysis of
Prayer Room Acoustics in the Pusdai
Mosque in West Java. Eng. Proc. 2023,
53, 4. https://doi.org/10.3390/
IOCBD2023-15187
Academic Editor: Alessandro
Cannavale
Published: 24 October 2023
Copyright:
© 2023 by the authors.
Licensee MDPI, Basel, Switzerland.
This article is an open access article
distributed
under
the
terms
and
conditions of the Creative Commons
Attribution (CC BY) license (https://
creativecommons.org/licenses/by/
4.0/).
Proceeding Paper
An Analysis of Prayer Room Acoustics in the Pusdai Mosque in
West Java †
Fadilatun Nur Latifah
, Rezky Sepvingga Putri, Fahman Syukur, Wida Hamidah Sutarno, Beta Paramita *
and Try Ramadhan
Architecture, Faculty of Technology and Vocational Education, Universitas Pendidikan Indonesia,
Bandung 40154, West Java, Indonesia; fadilatunnurlatifah@upi.edu (F.N.L.); sepvinggaputri@upi.edu (R.S.P.);
fahman@upi.edu (F.S.); widahamid@upi.edu (W.H.S.); tryramadhan@upi.edu (T.R.)
* Correspondence: betaparamita@upi.edu
† Presented at the 1st International Online Conference on Buildings, 24–26 October 2023; Available online:
https://iocbd2023.sciforum.net/.
Abstract: Aside from being a place for congregational prayers, the West Java Pusdai Mosque is also a
center for preaching and other Islamic activities in West Java. Therefore, as a place of worship for
Muslims, this mosque needs to maintain a comfortable atmosphere. The comfort or solemnity of
worship can be affected by the noise of the surrounding environment or the acoustics of the room.
This study aims to analyze the acoustic quality of the prayer room in the Pusdai Mosque, which
is inﬂuenced by several factors. This research was conducted by observing and simulating, using
the Ecotect v5.50 software. A simulation was carried out with the creation of a 3D model and the
addition of both the absorption coefﬁcient of the material in the room and speakers to it. In addition,
research was also strengthened by conducting literature studies on scientiﬁc articles. The simulation
was carried out to determine the reverberation time and sound distribution produced by the sound
sources or speakers that could indicate the acoustic quality of the Pusdai Mosque. The acoustic
quality of the Pusdai Mosque is greatly inﬂuenced by the interior materials and the shape of the
ceiling. Based on the results of this analysis, the Pusdai Mosque has room acoustic defects. This is
due to the large use of sound-reﬂecting materials and the form of the ceiling, which is quite complex.
This causes a lot of sound reﬂection to occur, causing the reverberation time to exceed the optimum
limit of a 500 Hz frequency (conversational space). This causes the speaker’s voice to become an echo
or hum. Therefore, the Pusdai Mosque needs to improve its room acoustics in order to create comfort
and solemnity in worship. Improvement can be made by adding sound-absorbing material.
Keywords: acoustic; mosque; Pusdai; Ecotect
1. Introduction
The Pusdai Mosque is located in a multi-mass area of religious buildings that is the
center of dakwah and Islamic activities in West Java. One concern for the Pusdai Mosque is
to create a comfortable atmosphere so that the congregation can worship solemnly. This
solemnity is inﬂuenced by several factors, both from the outside and from within the
mosque itself. If the noise that enters the building from the outside exceeds the standard, it
reduces the comfort of the congregation [1].
In addition, conditions inside the mosque also affect the comfort of worship. Mosques
need good-quality acoustic space so that the voice of the imam or khatib can be clearly heard
by the congregation. Room acoustics can be said to be good if the reverberation time of the
voice during speech or speaking reaches the optimum value and the sound distribution
is evenly distributed. Therefore, reverberation time is one of the most important factors
for determining the quality of room acoustics [2,3]. Room acoustics can be inﬂuenced by
several factors, such as loudspeakers, the shape and size of the room, the shape of the roof
or ceiling, the material, and the number of room users [4–6].
Eng. Proc. 2023, 53, 4. https://doi.org/10.3390/IOCBD2023-15187
https://www.mdpi.com/journal/engproc
Eng. Proc. 2023, 53, 4
2 of 9
Previous research states that tajug roofs or ceilings have better sound quality than
dome roofs but can cause reverberation if not built with the right acoustic material [7,8].
Materials with an optimum absorption coefﬁcient are good for room acoustics. Good
sound-absorbing materials are porous or ﬁbrous materials [9]. In addition, in other studies,
ﬂat ceilings and leveled ceilings can also evenly distribute sound [10].
The Pusdai Mosque has a unique spatial form. The Pusdai Mosque uses a combination
of two architectural styles, namely Sundanese architecture and Islamic architecture, by
using a combination of ﬂat roofs and four-stacked pyramid roofs [11]. In addition, the
corridor area is also unique because of its complex or leveled ceiling shape and is equipped
with a signiﬁcant number of loudspeakers hanging from the ceiling. Most of the materials
used in this mosque are brick, concrete, and granite. These things can certainly affect the
acoustic quality of the Pusdai Mosque.
Therefore, this study aims to examine and discuss the acoustic quality of the Pusdai
Mosque by involving several inﬂuential factors. This study will discuss the acoustic
conditions and reverberation time received by the congregation, which can affect the
solemnity of worship. In addition, this study will also discuss the inﬂuence of the shape of
the space, loudspeaker conditions, materials, and the capacity of the congregation on the
acoustics of the mosque.
2. Methods
This research analyzes the reverberation time and sound distribution through rays and
particles in the Pusdai Mosque prayer room. Simulation results are then compared with
acoustic standards. This research was conducted via ﬁeld observations and simulations
with Ecotect v5.50 software as well as literature studies in order to obtain the necessary
information and strengthen the research. Ecotect v5.50 was the software used to identify
reverberation time, delay time, and echo [12].
Before conducting acoustic simulations, background noise simulations were carried
out to replicate those that entered the building and to determine the comfort of the con-
gregation in response to outside noise. After that, a 3D model was simulated in Ecotect by
entering several factors. Next, the model was analyzed with regard to the sound distribu-
tion of each speaker through rays, particles, and the resulting reverberation time analysis.
From the simulation, the value was compared with acoustic standards to determine the
acoustic quality and comfort of the sound received by the congregation.
3. Results and Discussion
3.1. Background Noise
The Pusdai Mosque is located on Diponegoro Street, Cibeunying Kaler, Bandung City,
West Java. The mosque area is directly adjacent to major roads to the north, east, and
south, as shown in Figure 1. This potentially leads to high levels of background noise
entering the building and affecting the comfort of worship. Based on SNI 03-6386-2000, the
acceptable noise limit for places of worship is 30–35 dB(A) [13]. Background noise analysis
was conducted by taking ﬁeld measurements using the Noise Meter mobile application
and with a simulation using Ibana software ver 1.2 Rev. 122.
The average background noise from all sides entering the building is obtained from the
analysis. The graph in Figure 2 shows that the background noise value entering the Pusdai
Mosque prayer area is quite low and below the optimum limit (30–35 dB(A)), therefore
indicating that this background noise is safe and not disturbing. This is due to the location
of the Pusdai Mosque prayer area, which is quite far from the sound source.
Eng. Proc. 2023, 53, 4
3 of 9
, 53, 4 
 
Figure 1. Pusdai Mosque site plan. Source: (Pus
The average background noise from a
the analysis. The graph in Figure 2 shows
Pusdai Mosque prayer area is quite low and
fore indicating that this background noise
location of the Pusdai Mosque prayer area,
Figure 1. Pusdai Mosque site plan. Source: (Pusdai, 2022).
. Proc. 2023, 53, 4 
 
Figure 1. Pusdai Mosque site plan. Source: (Pusdai, 2022). 
The average background noise from all sides entering the buil
the analysis. The graph in Figure 2 shows that the background no
Pusdai Mosque prayer area is quite low and below the optimum limi
fore indicating that this background noise is safe and not disturbi
location of the Pusdai Mosque prayer area, which is quite far from t
 
Figure 2. Background noise level chart. 
3.2. Room Design 
The Pusdai Mosque has a fairly large space, measuring ± 60 m 
surrounding the main prayer area, as shown in Figure 3a. Based on
Secretary of the Pusdai Mosque Prosperity Board, Faturahman, thi
modate as many as 4600 people. The mosque has two ﬂoors, and th
is ±4 m; it also has a four-stacked pyramid roof with a height of 22 m
3b. 
Figure 2. Background noise level chart.
3.2. Room Design
The Pusdai Mosque has a fairly large space, measuring ± 60 m × 40 m, with corridors
surrounding the main prayer area, as shown in Figure 3a. Based on information from the
Secretary of the Pusdai Mosque Prosperity Board, Faturahman, this mosque can accommo-
date as many as 4600 people. The mosque has two ﬂoors, and the height of each ﬂoor is
±4 m; it also has a four-stacked pyramid roof with a height of 22 m, as shown in Figure 3b.
Eng. Proc. 2023, 53, 4
4 of 9
The Pusdai Mosque has a fairly large space, measuring ± 60 m × 40 m, with corridor
surrounding the main prayer area, as shown in Figure 3a. Based on information from th
Secretary of the Pusdai Mosque Prosperity Board, Faturahman, this mosque can accom
modate as many as 4600 people. The mosque has two ﬂoors, and the height of each ﬂoo
is ±4 m; it also has a four-stacked pyramid roof with a height of 22 m, as shown in Figur
3b. 
 
 
(a) 
(b) 
Figure 3. (a) Floor plan of Pusdai Mosque; (b) east elevation of Pusdai Mosque. Source: (Pusda
2022). 
The simulated 3D model was simpliﬁed due to the limitations of Ecotect [14]. Base
on Ecotect calculations, the prayer room at the Pusdai Mosque has a volume of 22,889.85
Figure 3. (a) Floor plan of Pusdai Mosque; (b) east elevation of Pusdai Mosque. Source: (Pusdai, 2022).
The simulated 3D model was simpliﬁed due to the limitations of Ecotect [14]. Based on
Ecotect calculations, the prayer room at the Pusdai Mosque has a volume of 22,889.850 m3
with a surface area of 30,196.316 m2. The Pusdai Mosque uses eight different types of
materials and 21 loudspeakers with the same speciﬁcations. From Figures 4 and 5, it can be
seen that most of the materials used in the Pusdai Mosque are brick, concrete, and granite.
. Proc. 2023, 53, 4 
4 o
m3 with a surface area of 30,196.316 m2. The Pusdai Mosque uses eight diﬀerent types
materials and 21 loudspeakers with the same speciﬁcations. From Figures 4 and 5, it c
be seen that most of the materials used in the Pusdai Mosque are brick, concrete, a
granite. 
 
 
 
(a) 
(b) 
(c) 
Figure 4. (a) Exterior material colors; (b) interior material colors; (c) description of colors. 
 
 
(a) 
(b) 
Figure 5. (a) The position of the 21 loudspeakers is indicated by the number inside the circle; (b)
side view of the speaker position marked in blue. 
Table 1 shows the absorption coeﬃcient of the materials used in the Pusdai Mosqu
Based on the data, it can be seen that the Pusdai Mosque uses many materials with lo
absorption coeﬃcients. This is because the material used is a type of material that reﬂe
sound. Sound will bounce if it hits hard, tight, and ﬁrm surfaces, such as concrete, bri
glass, granite, and PVC [17]. This likely aﬀects the acoustic quality of the Pusdai Mosqu
Table 1. Material absorption coeﬃcient. 
Materials 
Frequency (Hz) 
63
125
250
500
1000
2000
4000
8000
1600
Figure 4. (a) Exterior material colors; (b) interior material colors; (c) description of colors.
2023, 53, 4 
4 of 8 
m3 with a surface area of 30,196.316 m2. The Pusdai Mosque uses eight diﬀerent types of 
materials and 21 loudspeakers with the same speciﬁcations. From Figures 4 and 5, it can 
be seen that most of the materials used in the Pusdai Mosque are brick, concrete, and 
granite. 
 
 
 
(a) 
(b) 
(c) 
Figure 4. (a) Exterior material colors; (b) interior material colors; (c) description of colors. 
 
 
(a) 
(b) 
Figure 5. (a) The position of the 21 loudspeakers is indicated by the number inside the circle; (b) 
side view of the speaker position marked in blue. 
Table 1 shows the absorption coeﬃcient of the materials used in the Pusdai Mosque. 
Based on the data, it can be seen that the Pusdai Mosque uses many materials with low 
absorption coeﬃcients. This is because the material used is a type of material that reﬂects 
sound. Sound will bounce if it hits hard, tight, and ﬁrm surfaces, such as concrete, brick, 
glass, granite, and PVC [17]. This likely aﬀects the acoustic quality of the Pusdai Mosque. 
Table 1. Material absorption coeﬃcient. 
Materials 
Frequency (Hz) 
63
125
250
500
1000
2000
4000
8000
1600
Figure 5. (a) The position of the 21 loudspeakers is indicated by the number inside the circle; (b) side
view of the speaker position marked in blue.
Table 1 shows the absorption coefﬁcient of the materials used in the Pusdai Mosque.
Based on the data, it can be seen that the Pusdai Mosque uses many materials with low
absorption coefﬁcients. This is because the material used is a type of material that reﬂects
sound. Sound will bounce if it hits hard, tight, and ﬁrm surfaces, such as concrete, brick,
glass, granite, and PVC [15]. This likely affects the acoustic quality of the Pusdai Mosque.
Eng. Proc. 2023, 53, 4
5 of 9
Table 1. Material absorption coefﬁcient.
Materials
Frequency (Hz)
63
125
250
500
1000
2000
4000
8000
1600
Granite
0.01
0.01
0.01
0.01
0.01
0.02
0.02
0.02
0.02
Carpet
0.08
0.08
0.08
0.30
0.60
0.75
0.80
0.80
0.70
Wood
0.18
0.18
0.12
0.10
0.09
0.08
0.07
0.07
0.06
Gypsum
0.29
0.29
0.10
0.05
0.04
0.07
0.09
0.09
0.08
PVC
0.01
0.01
0.01
0.02
0.02
0.02
0.02
0.02
0.01
Brick
0.10
0.07
0.03
0.02
0.02
0.02
0.03
0.02
0.03
Concrete
0.12
0.09
0.07
0.01
0.01
0.01
0.02
0.01
0.02
Glass
0.18
0.15
0.10
0.03
0.01
0.01
0.01
0.02
0.02
Source: [16,17].
3.3. Reverberation Time
Ecotect was used to analyze the reverberation time, after inputting the type of material
and the number of speakers used in the room. The Pusdai Mosque uses a signiﬁcant
number of loudspeakers for the prayer area, namely 10 loudspeakers under the ceiling of
the ﬁrst-ﬂoor corridor, seven loudspeakers under the ceiling of the second ﬂoor, and four
loudspeakers mounted on the walls around the main prayer area.
Figure 5a,b shows the positions of the speakers on Floors 1 and 2 of the Pusdai Mosque.
All of the speakers used have the same type and frequency, which is 500 Hz, with a range
width or azimuth angle of 180◦ and axial rotation angel of 45◦. Figure 6a shows the
distribution pattern of the sound produced by the speakers.
5 of 8 
Figure 5a,b shows the positions of the speakers on Floors 1 and 2 of the Pusdai 
Mosque. All of the speakers used have the same type and frequency, which is 500 Hz, with 
a range width or azimuth angle of 180° and axial rotation angel of 45°. Figure 6a shows 
the distribution paern of the sound produced by the speakers. 
Figure 6b shows the reverberation time from the Ecotect simulation results. The 
graph in Figure 6b shows the diﬀerence in reverberation time with diﬀerent percentages 
of the number of congregations, starting from 0%, 30%, and 100% of the capacity of a 4600-
person congregation. The simulation was carried out at a percentage of 30% capacity, be-
cause the Pusdai Mosque is often only ﬁlled with about 30% of the total congregation 
capacity that the mosque can hold (Faturahman, 2020). 
 
 
(a) 
(b) 
Figure 6. (a) Loudspeaker sound distribution paern; (b) reverberation time chart. 
Based on the recommendation from Ecotect, this reverberation time calculation uses 
the Norris–Eyring formula. The optimum value of the reverberation time suitable for 
speech (500 Hz) at the Pusdai Mosque is 1.21 s. The simulation results show that the acous-
tic quality of the Pusdai Mosque is not good, because the average reverberation time value 
exceeds the optimum value, especially at the center frequency (500 Hz) that is generally 
used for speech. 
Table 2 shows that the reverberation time at frequencies of 125–16,000 Hz is far from 
the optimum reverberation time. This is caused by several factors. When viewed from the 
shape of the room, the shape of the Pusdai Mosque room provides a good acoustic re-
sponse. This can be seen from the shape of the leveled ceiling and terraced pyramid roof. 
However, the Pusdai Mosque uses a lot of sound-reﬂecting materials such as the use of 
concrete on the ﬁrst-ﬂoor ceiling; granite for the ﬂoor and front wall; and the use of brick, 
concrete, and glass walls [17]. 
Figure 6. (a) Loudspeaker sound distribution pattern; (b) reverberation time chart.
Figure 6b shows the reverberation time from the Ecotect simulation results. The graph
in Figure 6b shows the difference in reverberation time with different percentages of the
number of congregations, starting from 0%, 30%, and 100% of the capacity of a 4600-person
congregation. The simulation was carried out at a percentage of 30% capacity, because the
Pusdai Mosque is often only ﬁlled with about 30% of the total congregation capacity that
the mosque can hold (Faturahman, 2020).
Based on the recommendation from Ecotect, this reverberation time calculation uses
the Norris–Eyring formula. The optimum value of the reverberation time suitable for
speech (500 Hz) at the Pusdai Mosque is 1.21 s. The simulation results show that the
acoustic quality of the Pusdai Mosque is not good, because the average reverberation
time value exceeds the optimum value, especially at the center frequency (500 Hz) that is
generally used for speech.
Eng. Proc. 2023, 53, 4
6 of 9
Table 2 shows that the reverberation time at frequencies of 125–16,000 Hz is far from
the optimum reverberation time. This is caused by several factors. When viewed from
the shape of the room, the shape of the Pusdai Mosque room provides a good acoustic
response. This can be seen from the shape of the leveled ceiling and terraced pyramid roof.
However, the Pusdai Mosque uses a lot of sound-reﬂecting materials such as the use of
concrete on the ﬁrst-ﬂoor ceiling; granite for the ﬂoor and front wall; and the use of brick,
concrete, and glass walls [17].
Table 2. Reverberation time.
Reverberation Time
Optimum (s)
Frequency
(Hz)
Absorption
Total (m2)
Percentage of Congregation
0% (s)
30% (s)
100% (s)
1.21
63
23,132.95
1.11
1.08
1.01
125
22,736.88
1.48
1.43
1.33
250
22,186.50
1.93
1.69
1.30
500
21,645.49
10.11
6.01
3.09
1000
21,132.99
9.70
6.13
3.30
2000
20,384.00
9.64
5.72
2.93
4000
19,293.25
5.81
4.69
3.23
8000
17,524.90
9.64
7.48
4.91
16,000
14,331.40
5.92
5.23
4.10
This causes the sound produced from the source to experience a lot of reﬂection,
resulting in a long-lasting reverberation time. This reduces the clarity of the voice of the
preacher or imam. Accordingly, the prayer room at the Pusdai Mosque, which has a volume
of 22,889.850 m3, also causes the sound to reverberate or buzz, as it was not designed using
acoustic materials.
In addition, the number of space users or congregations also affects the reverberation
time of the sound. The more users, the smaller the reverberation time produced. At a
frequency of 500 Hz, the reverberation time produced when the percentage of congregants
is 100% (4600 congregants) is 3.09 s. Meanwhile, when the percentage of congregants is
only 30% (1380 congregants), the reverberation time increases to 6.01 s. This is because
the human body is also a sound absorber [18]. However, in Figure 6b and Table 2, the
reverberation time generated at the Pusdai Mosque is shown to still exceed the optimum
value, despite being ﬁlled to full congregation capacity (4600 congregants). This indicates
that the Pusdai Mosque has poor acoustic quality, which results in the voice of the preacher
or imam sounding less clear and echoing or reverbing.
3.4. Rays and Particles
A simulation of rays and particles is useful for seeing the spread or travel of sound
produced by each speaker. From this simulation, the effect of the shape of the space and
material on sound reﬂection can also be seen. In addition, this simulation shows the areas
that are effective for generating sound or producing echoes or reverb.
Figure 7a–d shows the spread of sound with four loudspeakers facing the main prayer
room—namely loudspeakers 1, 2, 3, and 4—with the setting of generate rays using a circular
pattern, an angular increment of 5.0◦, and as many as eight bounces. This is in line with
what Ridhatiana accomplished during a simulation of acoustics at the Al-A’zhom Grand
Mosque, Tangerang City. The range of normal bounces is 8–32 bounces [19].
Eng. Proc. 2023, 53, 4
7 of 9
Figure 7a d shows the spread of sound with four loudspeakers facing the main 
prayer room—namely loudspeakers 1, 2, 3, and 4—with the seing of generate rays using 
a circular paern, an angular increment of 5.0°, and as many as eight bounces. This is in 
line with what Ridhatiana accomplished during a simulation of acoustics at the Al-
A’zhom Grand Mosque, Tangerang City. The range of normal bounces is 8–32 bounces 
[19]. 
 
 
(a) 
(b) 
 
 
(c) 
(d) 
 Direct
 Useful 
 Border
 Echo 
 Reverb 
 Masked 
Figure 7. (a) Sound spread loudspeaker 1; (b) sound spread loudspeaker 2; (c) sound spread loud-
speaker 3; (d) sound spread loudspeaker 4. 
The simulation results show that the sound distribution produced by the four loud-
speakers still produces a signiﬁcant amount of reverb sound in the middle area to the roof, 
which is the area that causes the sound to reverb. The shape of the space can aﬀect the 
direction of sound dispersion produced by the loudspeaker. The four ﬁgures show that a 
multilevel pyramid-shaped ceiling creates a diﬀuse sound reﬂection. This is good for 
room acoustics. However, the material used also needs to be considered so as to not pro-
duce echoing or reverberating sounds. Most of the ceilings at the Pusdai Mosque use con-
crete and PVC materials, which have a low absorption coeﬃcient. 
Figure 7. (a) Sound spread loudspeaker 1; (b) sound spread loudspeaker 2; (c) sound spread
loudspeaker 3; (d) sound spread loudspeaker 4.
The simulation results show that the sound distribution produced by the four loud-
speakers still produces a signiﬁcant amount of reverb sound in the middle area to the roof,
which is the area that causes the sound to reverb. The shape of the space can affect the
direction of sound dispersion produced by the loudspeaker. The four ﬁgures show that a
multilevel pyramid-shaped ceiling creates a diffuse sound reﬂection. This is good for room
acoustics. However, the material used also needs to be considered so as to not produce
echoing or reverberating sounds. Most of the ceilings at the Pusdai Mosque use concrete
and PVC materials, which have a low absorption coefﬁcient.
In addition, the ceiling in the corridor area around the prayer area also has a complex
and leveled shape, as shown in Figure 8a.
ng. Proc. 2023, 53, 4 
7 of 
In addition, the ceiling in the corridor area around the prayer area also has a complex
and leveled shape, as shown in Figure 8a. 
 
 
 
(a) 
(b) 
(c) 
Figure 8. (a) Corridor ceiling design; (b) sound spread of loudspeaker 11 in the corridor; (c) speake
11 sound spread due to ceiling design. 
Figure 8b shows that the spread of sound from one of the loudspeakers (loudspeake
11) that hangs on the ceiling of the corridor causes many reﬂections that produce reverb
sounds. In addition, the sound produced is only centered under the speaker. The shape o
the ceiling in this corridor results in diﬀuse sound reﬂections, as shown in Figure 8c. How
ever, due to its complex shape and the use of reﬂective materials, there is a signiﬁcan
amount of sound reﬂection, resulting in reverberation. 
4. Conclusions 
Based on the analysis conducted, it can be concluded that the Pusdai Mosque has a
spatial acoustic defect because the reverberation time exceeds the optimum time. Back
d
i
i
th t
t
th b ildi
h
ﬀ
t
th
ti
f th
Figure 8. (a) Corridor ceiling design; (b) sound spread of loudspeaker 11 in the corridor; (c) speaker
11 sound spread due to ceiling design.
Figure 8b shows that the spread of sound from one of the loudspeakers (loudspeaker
11) that hangs on the ceiling of the corridor causes many reﬂections that produce reverb
sounds. In addition, the sound produced is only centered under the speaker. The shape
of the ceiling in this corridor results in diffuse sound reﬂections, as shown in Figure 8c.
However, due to its complex shape and the use of reﬂective materials, there is a signiﬁcant
amount of sound reﬂection, resulting in reverberation.
Eng. Proc. 2023, 53, 4
8 of 9
4. Conclusions
Based on the analysis conducted, it can be concluded that the Pusdai Mosque has a
spatial acoustic defect because the reverberation time exceeds the optimum time. Back-
ground noise or noise that enters the building has no effect on the acoustic space of the
Pusdai Mosque because the noise level is low.
Based on the results of the analysis obtained, the acoustic space at the Pusdai Mosque
is strongly inﬂuenced by the volume of space, the number of congregations, the shape of
the ceiling, and the materials used. Most of the materials used at the Pusdai Mosque are
sound-reﬂecting materials, such as brick, concrete, glass, granite, and PVC. These materials
have a low sound absorption coefﬁcient. This excessive sound reﬂection eventually causes
the sound to echo or hum. This reduces the clarity of the sound. The congregation or
listeners are less able to properly and clearly hear the voice of the preacher or imam.
This can lead to reduced solemnity in worship. Therefore, it is necessary to improve
the acoustic design of the Pusdai Mosque. Acoustic improvements can be made by adding
sound-absorbing materials. For example, granite on the ﬂoor can be replaced with a layer
of parquet wood, which has a better absorption coefﬁcient [19,20]. In addition, the shape of
the ceiling can be changed to a less complex shape to avoid many sound reﬂections.
Further research and simulations are needed to discover the best acoustic modeling
for the Pusdai Mosque. This is because acoustics greatly affect the comfort or quality of the
speaker’s voice received by the listener.
Author Contributions: Conceptualization, F.N.L.; methodology, F.N.L.; software, F.N.L. and F.S.;
validation, F.N.L. and R.S.P.; formal analysis, F.N.L. and R.S.P.; investigation, F.N.L. and W.H.S.;
resources, F.N.L.; data curation, F.N.L.; writing—original draft preparation, F.N.L.; writing—review
and editing, F.N.L., B.P. and T.R.; visualization, F.N.L.; supervision, B.P. and T.R.; project admin-
istration, B.P. and T.R.; funding acquisition, B.P. and T.R. All authors have read and agreed to the
published version of the manuscript.
Funding: This research was funded by BeCool Indonesia.
Institutional Review Board Statement: Not applicable.
Informed Consent Statement: Not applicable.
Data Availability Statement: Data are obtained in the article.
Acknowledgments: This paper was supported by the Center of Excellence for Low Emission Building
Materials and Energy and the Laboratory of Science, Technology and Building Materials of the
Universitas Pendidikan Indonesia.
Conﬂicts of Interest: The authors declare no conﬂict of interest.
References
1.
De Salvio, D.; D’Orazio, D.; Garai, M. Unsupervised analysis of background noise sources in active ofﬁces. J. Acoust. Soc. Am.
2021, 149, 4049–4060. [CrossRef] [PubMed]
2.
Eaton, J.; Gaubitch, N.D.; Moore, A.H.; Naylor, P.A. Estimation of Room Acoustic Parameters: The ACE Challenge. IEEE/ACM
Trans. Audio Speech Lang. Process. 2016, 24, 1681–1693. [CrossRef]
3.
Kusuma, R.B.I.; Suyatno, S.; Prajitno, G. Analisis dan Simulasi Optimasi Parameter Akustik Ruang pada Smart Classroom
Departemen Fisika ITS. J. Sains Dan Seni ITS 2021, 10, B7–B14. [CrossRef]
4.
Cahyono, R. Evaluasi Akustik Ruang dan Tata Suara pada Gedung Graha Patria Kota Blitar. Undergraduate Thesis, Institut
Teknologi Sepuluh Nopember, Surabaya, Indonesia, 2018.
5.
Baikhaqi, M.I. Desain Akustik Ruang Pada Home Theater Multifungsi Perpustakaan ITS. Undergraduate Thesis, Institut Teknologi
Sepuluh Nopember, Surabaya, Indonesia, 2015.
6.
Sampurna, R. Pengaruh Penampang Asimetris Terhadap Kinerja Akustik Pada Ruang Audio Visual Gedung G Fakultas Teknik
Universitas Telkom. 2016. Available online: https://repository.telkomuniversity.ac.id/home/catalog/id/116684/slug/pengaruh-
penampang-asimetris-terhadap-kinerja-akustik-pada-ruang-audio-visual-gedung-g-fakultas-teknik-universitas-telkom.html
(accessed on 9 January 2023).
7.
Fauji. Evaluasi kinerja akustik ruang pada masjid dengan bentuk plafon tajug. Undergraduate Thesis, Institut Teknologi Sepuluh
Nopember, Surabaya, Indonesia, 2017.
Eng. Proc. 2023, 53, 4
9 of 9
8.
Bena, E.F.; Arsitektur, F.T.J.; Sudarmo, B.S.; Ridjal, A.M. Waktu Dengung Ruang Sholat Masjid Desa Berdasarkan Perbedaan
Bentuk Plafon. Rev. Urban. Arch. Stud. 2014, 12, 41–53. [CrossRef]
9.
Putra, A.R.; Nazhar, R.D. Peranan Material Interior dalam Pengendalian Akustik Auditorium Bandung Creative Hub. Waca Cipta
Ruang 2020, 6, 71–76. [CrossRef]
10.
Yani, Y. Penilaian kualitas akustik masjid Raudhaturrahmah Padang Tiji dengan menggunakan simulasi Ecotect. J. Arsit. Pendapa
2021, 4, 19–27. [CrossRef]
11.
Kustianingrum, D.; Rozi, A.; Mulyanidya, F.; Firdaus, F. Kajian Tatanan Massa dan Bentuk Bangunan Pusat Dakwah Islam
Bandung. Reka Karsa J. Arsit. 2014, 2, 1–13.
12.
Aldona, N.; Seftyarizki, D.; Prihatiningrum, A.; Ramawangsa, P.A.; Khairunnisa, E.; Refti, S.M.; Kharisma, M.W. Identiﬁcation of
Acoustic Comfort in Classroom of Gedung Kuliah Bersama V of Bengkulu University. IOP Conf. Ser. Earth Environ. Sci. 2021.
[CrossRef]
13.
SNI 03-6386-2000; Spesiﬁkasi Tingkat Bunyi Dan Waktu Dengung Dalam Bangunan Gedung Dan Perumahan (Kriteria Desain
Yang Direkomendasikan). Badan Standarisasi Nasional: Jakarta, Indonesia, 2000; p. 18.
14.
Indrani, H.C.; Ekasiwi, S.N.N.; Asmoro, W.A. Aplikasi Model Komputer Dalam Analisis Kinerja Akustik Ruang Auditorium
Universitas Kristen Petra Surabaya. Dimens. Inter. 2007, 5, 109–121. Available online: http://puslit2.petra.ac.id/ejournal/index.
php/int/article/view/16882 (accessed on 26 February 2023).
15.
Dewi, N.U.I.; Syamsiyah, N.R. Kualitas Akustik Ruang Utama Masjid Siti Aisyah Surakarta. Sinektika J. Arsit. 2020, 16, 73–79.
[CrossRef]
16.
Sü, Z.; Çalı¸skan, M. Acoustical Design and Noise Control in Metro Stations: Case Studies of the Ankara Metro System. Build.
Acoust. 2007, 14, 203–221. [CrossRef]
17.
Ansay, S.; Zannin, P.H.T. Using the parameters of deﬁnition, D50, and reverberation time, RT, to investigate the acoustic quality
of classrooms. Can. Acoust. 2016, 44, 6–11.
18.
Syamsiyah, N.R.; Utami, S.S.; Dharoko, A. Kualitas Akustik Ruang Pada Masjid Berkarakter Opening Wall Design. RAPI XIII
Simp. Nas. 2014, 66–74. Available online: http://duniaakustik.wordpress.com/ (accessed on 6 January 2023).
19.
Ridhatiana, N.S. Tata Akustik Ruang Masjid Raya Al-a’Zhom Kota Tangerang. Ph.D. Thesis, Universitas Brawijaya, Malang,
Indonesia, 2021. Available online: http://arsitektur.studentjournal.ub.ac.id/index.php/jma/article/view/1545 (accessed on
6 January 2023).
20.
Setiawan, D.M. Optimalisasi Performa Akustik Ruang pada Ruang Ibadah Utama di Gereja Khatolik Paroki Santo Thomas Kelapa Dua Depok
Jawa Barat; Universitas Atma Jaya Yogyakarta: Yogyakarta, Indonesia, 2017; Available online: https;//e-journal.uajy.ac.id/11914/
(accessed on 6 January 2023).
Disclaimer/Publisher’s Note: The statements, opinions and data contained in all publications are solely those of the individual
author(s) and contributor(s) and not of MDPI and/or the editor(s). MDPI and/or the editor(s) disclaim responsibility for any injury to
people or property resulting from any ideas, methods, instructions or products referred to in the content.
