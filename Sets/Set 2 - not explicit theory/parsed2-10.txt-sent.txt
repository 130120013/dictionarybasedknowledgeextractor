Unfortunately, architects nowadays often focus more on designing a building based on its looks or form, and the 
main function of space most often neglected.
Published by Elsevier Ltd. 
Peer-review under responsibility of AMER (Association of Malaysian Environment-Behaviour Researchers) and cE-Bs (Centre 
for Environment-Behaviour Studies, Faculty of Architecture, Planning & Surveying, Universiti Teknologi MARA, Malaysia.
Introduction  
Architectural acoustics is the art of achieving a good sound within a building.
Based on Wikipedia the first 
application of modern scientific methods to architectural acoustics was carried out by Wallace Sabine in the Fogg 
 
 
* Corresponding author.
/  Procedia - Social and Behavioral Sciences  234 ( 2016 )  45  54 
Museum lecture room.
He then applied his new found knowledge to the design of Symphony Hall in Boston.
Architectural acoustics is about achieving a good quality of speech in a theatre, improvise quality of music in a 
concert hall or recording studio, or suppressing noise to make offices and homes more comfortable and peaceful 
places to work and live.
A high level of quality of sound is needed for all worship activities.
Statement of the problem 
Architects now days often concentrate more on designing a building based on its looks or the forms and the main 
function of space have been put aside.
Purpose of the study 
The purpose of the study is to identify the importance of acoustic design in the mosque towards the comfort of 
the worshipers.
Aim and objectives of research 
The aim of this study is to identify the importance of acoustic design in the Masjid Al-Hussain at Kuala Perlis, 
Perlis towards the comfort of the worshipers.
Therefore, it is expected by the end of this study; researcher has come to the conclusion on the importance of 
acoustic design in the mosque towards the comfort of the worshipers.
Introduction of building acoustic 
Building acoustic is considered to be one of the most important aspects of building design that prioritises 
speeches and music, primarily buildings such as concert halls and places of worship such as mosques.
As studied by 
Lidia lvarez-Morales, building acoustic is typically defined from which the positions of the source and positions of 
listeners in the various zones is analysed by processing acoustic parameters related to reverberation, sound strength, 
clarity, early lateral reflections, and the speech intelligibility.
Meanwhile, as researched by Ismail (2013), rooms for 
47
 Ahmad Ridzwan Othman et al.
/  Procedia - Social and Behavioral Sciences  234 ( 2016 )  45  54 
musical performance shows a growing understanding of the more important acoustical characteristics of the concert 
(such as bass effect) and are well defined although the need to improve tools for applying this knowledge to the 
design of these spaces is still in progress.
On the other hand, rooms for speech are not as well defined because of 
special groups of listener such as the very young or old, hearing impaired listeners, and those listening in a second 
language.
From here, it can be seen that building acoustic is divided into two categories, speech and music.
Speech 
The study by Ismail (2013) speech intelligibility is affected by an excessive reverberation time, (T60).
Sabines 
formula is usually used to calculated (T60) from the empirical relation: (T60) = 0.16V/A in seconds, where V is the 
volume (m3) of the auditorium, and A is the total absorption (m2).
He also stated that, an advanced technique, introduced by Schroeder, uses the squared 
reverse-integrated impulse response of an auditorium (which is equivalent to the ensemble average of an infinite 
number of squared reverberation decays).
In conclusion, this technique is useful for quick calculation of the EDT, 
and can be used for in-situ applications.
Music 
In musical acoustics, the room should be able to enhance the quality of the musical reverberation, whereas 
studied by Bradley (2009) the room should strongly reproduce bass sounds for most types of musical performances.
Moreover, the rooms for musical performances 
should have increased low-frequency reverberation times, where the further study shows that typically the 125 Hz 
reverberation time is required to be 1.5 times the mid-frequency value.
It sometimes leads to quite extreme efforts to 
reduce low-frequency absorption in halls.
Mosques are a multi-function public hall with many worshipping 
activities that have different acoustical requirements (Khabiri et al., 2013).
In a study by Zhre S Gl and Mehmet 
alskan  (2013), they found that having a high quality of sound is an important feature in main prayer hall design, 
and it is a fundamental element in mosques and various places of worship.
Although that is true, studies by Ismail (2013), stressed that the importance of speech intelligibility became more 
important in contemporary mosque design, that the integration of other activities such as the Holy Quran recitation, 
speeches, and lectures in the prayer halls is a norm.
It means that acoustical design is crucial within mosques to 
ensure that the quality of sound, whether in term of speech or music, is maintained to ensure religious activities can 
proceed more smoothly.
Effect of  reverberation time on mosque 
In a study by Zhre S Gl and Mehmet alskan (2013), they found that reverberation time, in scientific term is 
defined as the time required for the average sound energy density to decay by 60 dB from an equilibrium level after 
the sound source has stopped.
Since 1900, Sabine has studied the phenomenon, and RT has been used as the main 
technique for discovering the acoustic characteristics of rooms environment and, in the case of the holy mosque, 
clues on intelligibility and sound aesthetics.
In Doramaczade Ali Paa Mosque acoustical simulations of T30 over 
frequency range are assumed as a global average of the interior volume and presented for each receiver location in 
the form of distribution maps.
As studied by Zhre S Gl and Mehmet alskan (2013) found that the result of global estimates indicate that 
perforated wood application on inverse triangular pendentive surfaces is effective both for occupied and unoccupied 
48  
 Ahmad Ridzwan Othman et al.
/  Procedia - Social and Behavioral Sciences  234 ( 2016 )  45  54 
conditions of the mosque, particularly in the improvement of lower to mid frequency range.
They also found that 
being the selected alternative design solution all other simulation results are presented only for perforated wood 
application on pendentive surfaces.
Average mid frequency T30 for an unoccupied mosque with acoustical 
treatment on pendentive surfaces is 1.94 s, and average low-frequency T30 is 2.07 s. As studied by Mazloomi 
(2010) the unique of this type of pendentive surfaces construction is that it creates massive multivolume space to the 
mosque's interior.
Lastly, they found that although speech is the main activity in a mosque, the low frequency and 
spiritual context of the male imams voice proved rather different than the ordinary speech, making it advisable to 
have a BR that is closer to optimal for music.
Room acoustic parameters and allowable limits (occupied spaces) 
 
 
 
 
 
Source:  Zhre S Gl, & Mehmet alskan  (2013) 
2.5.
Spaces 
As studied by Ismail (2013), the selection criteria were set to maintain various cross relationship geometrical 
parameters with increasing mosque size.
The relationship of the mosque plot area and volume is shown in Figure 1.
Evidently, the linear relationship demonstrated by the graph proves that the selected mosques maintain a sheer 
volume and plot area relationship.
The volume increases with increasing plinth area.
This linear relationship will 
ensure a linear increase in RT with the increase in volume.
He also studied about the linear increase in volume with increasing surface area is represented Figure 2.
The 
relative data are represented regarding its relationship to the plinth area of the mosque prayer hall.
The strong 
relationship between the effective surface areas and the surface area of the plan is shown by the solid circle lines.
However, the resultant volume from the integration of these surface areas resulted in a relative increase in the 
effective volume to plan the surface area ratio for some geometry.
This result is most probably due to the presence 
of the dominant hemispherical domes in the main prayer and daily prayer halls of the mosque.
/  Procedia - Social and Behavioral Sciences  234 ( 2016 )  45  54 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Fig.1.Volume plinth area relationship  
Source: (Ismail, 2013) 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Fig.2.
Prayer halls geometrical parameters relationship 
Source: (Ismail, 2013) 
2.6.
Shaded area effective acoustical dome area of Doramaczade Ali Paa Mosque (c: center, r: radius, f: sample focal point).
Source : b. Zhre S Gl a, , Mehmet als kan c (2013) 
 
Speech intelligibility is the primary acoustical concern and necessitates suitable designation of volume, the 
geometry of the main space and appropriate use of acoustically absorptive and diffusive materials as finish surfaces 
(Zhre S Gl & Mehmet alskan, 2013).
As previously mentioned, the dome is 
the symbolic shelter element of traditional mosque model.
He studied the case of Doramaczade Ali Paa Mosque, 
50  
 Ahmad Ridzwan Othman et al.
The ratio of the effective surface area to the effective volume for the three prayer halls 
is plotted in Figure 4.
The effect of the large hemispherical domes in reducing the surface area is represented by the 
solid square plots.
The upper curves in Table 4 clearly show the low surface area/volume (SA/V) ratio of the sphere 
compared with the other geometrical shapes.
The positioning of the central chiller units or stand-alone split 
units inside designated spaces or on rooftops amplifies the background noise levels inside prayer areas and affects 
the acoustic spatial tranquility of space.
Role of acoustic design towards the comfort of the worshipper 
As studied by Berardo Naticchia (2007), buildings are often inserted in highly inhabited urban areas, near 
infrastructures and plants radiating high noise levels, the strategic importance of this task is increasing, together with 
the importance of acoustic comfort inside buildings.
Research methodology 
In this research, the method that will be used is a qualitative method.
The qualitative method investigates the why 
and how in this topic, based on analysis of information such as interviews and group discussions, observation and 
reflection field notes, various texts, pictures, and other materials.
According to McNamara (1999), who also 
mentioned that interviews are particularly useful for getting the story behind a participant's experiences.
The first part explains the surveys, which also included on the scope of study upon 
mosque with acoustic designs and the choice of Masjid Al-Hussain as the mosque chosen in Perlis.
While, on the 
other hand, the second part is divided into two phases  preliminary study and special study that also included 
methods for the data collection.
Case study, Masjid Al-Hussain  
The sample mosque was Masjid Al-Hussain situated in Kuala Perlis, Perlis.
The study was carried out on various 
age groups of worshipers that are at the mosque with an age range of six to seventy years old.
The main prayer hall has only one door with eight side windows and another window on the qibla wall.
Instruments 
This study was carried out through interviews, observations and behaviour mapping.
Furthermore, the researcher 
used the participatory technique in conducting this research on worshiper behaviour.
According to (Seamon, 2000) 
as cited by (Azlina & Zulkiflee, 2012), this approach discovers and defines the situation or experiences of people 
with the environment.
The study was through observations of worshipers during the 
Jemaah prayer and motivation lectures.
The worshipers were also interviewed through several questions to facilitate 
a more precise answer.
Interviews 
The second instrument of the study is through interviews.
The reason for choosing 
Maghrib time to conduct the research is because the number of worshipers is more compares to the other times.
The case study 
The case study of the research is a mosque located at Kuala Perlis, Perlis.
The mosque has a 50-meter bridge 
connected to the main prayer hall above the water as seen in Figure 5.
50 meter bridge connects to the main prayer hall 
4.2.
As studied by Ismail (2013), the circular shape and quarter spherical top of the 
Mihrab was geometrically developed to improve the reflected sound component towards the worshippers direction.
The main prayer hall has only one door with eight side windows and another one window on the qibla wall.
Based on the research have been done, the dome is made from two 
layers of aluminium, with a layer of polyvinylidene difluoride (PVDF) protecting its surface.
The floor of the main 
prayer hall also fully covered with carpets in Figure 8.
Carpeted Main Prayer Hall            Fig.
10.The above the Main Prayer Hall              Fig.
Comfort of worshipers 
It has been observed that when the researcher enters the main prayer hall the noised from outside such as the 
wave of the sea, people voices, the noised from the boats at the sea and the vehicles from the main road was not 
heard.
