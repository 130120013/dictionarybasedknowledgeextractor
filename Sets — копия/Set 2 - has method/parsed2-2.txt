See discussions, stats, and author profiles for this publication at: https://www.researchgate.net/publication/234812890
Beam Tracing Polygonal Objects
Article  in  ACM SIGGRAPH Computer Graphics · September 1998
DOI: 10.1145/964965.808588 · Source: CiteSeer
CITATIONS
259
READS
573
2 authors, including:
Paul S. Heckbert
Carnegie Mellon University
76 PUBLICATIONS   9,502 CITATIONS   
SEE PROFILE
All content following this page was uploaded by Paul S. Heckbert on 04 April 2015.
The user has requested enhancement of the downloaded file.
BEAM TRACING POLYGONAL OBJECTS
Paul S. Heckbert
Pat Hanrahan
y
N
Computer Graphics Laborator
ew York Institute of Technology
Old Westbury, NY 11568
Abstract
c
c
Ray tracing has produced some of the most realisti
omputer generated pictures to date. They contain surface
.
T
texturing, local shading, shadows, reﬂections and refractions
he major disadvantage of ray tracing results from its point-
t
e
sampling approach. Because calculation proceeds ab initio a
ach pixel it is very CPU intensive and may contain notice-
-
t
able aliasing artifacts. It is difﬁcult to take advantage of spa
ial coherence because the shapes of reﬂections and refrac-
tions from curved surfaces are so complex.
In this paper we describe an algorithm that utilizes the
f
spatial coherence of polygonal environments by combining
eatures of both image and object space hidden surface algo-
,
w
rithms.
Instead of tracing inﬁnitesimally thin rays of light
e sweep areas through a scene to form ‘‘beams.’’ This tech-
r
t
nique works particularly well for polygonal models since fo
his case the reﬂections are linear transformations, and refrac-
tions are often approximately so.
The recursive beam tracer begins by sweeping the pro-
a
jection plane through the scene.
Beam-surface intersections
re computed using two-dimensional polygonal set operations
h
and an occlusion algorithm similar to the Weiler-Atherton
idden surface algorithm.
For each beam-polygon intersec-
r
tion the beam is fragmented and new beams created for the
eﬂected and transmitted swaths of light.
These sub-beams
-
s
are redirected with a 4x4 matrix transformation and recur
ively traced. This beam tree is an object space representa-
h
tion of the entire picture.
hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
Appeared in Proc. SIGGRAPH ’84, pp. 119-127, July 1984. Minor typos have been
corrected. This version is missing all ﬁgures.
Since the priority of polygons is pre-determined, the
s
ﬁnal picture with reﬂections, refractions, shadows, and hidden
urface removal is easily drawn. The coherence information
.
I
enables very fast scan conversion and high resolution output
mage space edge and texture antialiasing methods can be
C
applied.
R Categories: I.3.3 [Computer Graphics]: Picture/Image
:
T
Generation - display algorithms; I.3.7 [Computer Graphics]
hree-Dimensional
Graphics
and
Realism
-
visible
G
line/surface algorithms.
eneral Terms: algorithms.
Additional Key Words and Phrases: ray tracing, refraction,
1
polygon, object space, coherence.
. Introduction
Two of the most popular methods used to create frame
-
i
buffer images of three-dimensional environments are ray trac
ng and scan line algorithms. Ray tracing generates a picture
p
by casting a ray of light from the eye point through each
ixel of the image and into the scene. Visible surfaces are
t
determined by testing for line-surface intersections between
he ray and each object in the scene. By recursively tracing
e
a
reﬂected and refracted rays, considerable realism can b
dded to the ﬁnal image. In contrast, a scan line rendering
-
f
program generally takes advantage of coherence to draw sur
aces incrementally. Comparing the two approaches we ﬁnd:
h
A
hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
dvantages of ray trace:
Uses a global lighting model that calculates
C
reﬂections, refractions, shadows.
an handle a variety of geometric primitives.
Disadvantages of ray trace:
Often slow since the intersection calculations
P
are ﬂoating point intensive.
oint sampling the environment causes aliasing.
Advantages of scan line algorithms:
Incremental calculation of geometry
D
is very efﬁcient.
isadvantages of scan line:
.
h
Local lighting model not as realistic
hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
-
c
Scan conversion of polygons is particularly popular, espe
ially in computer animation, where many images must be
a
m
produced and image generation time must be kept to
inimum [Crow, 1978].
Ray tracing has generated some
l
f
very realistic still images, but generally has been impractica
or animation. The only extensive ray traced animation made
e
[
to date was done with the aid of special—purpose hardwar
Kawaguchi, 1983; Nishimura et. al., 1983].
-
r
The differences between many hidden surface algo
ithms depend on the techniques used to exploit coherence in
-
i
the scene. Most attempts at exploiting coherence in ray trac
ng have concentrated on techniques to limit the number of
d
ray-surface intersections that are to be tested.
This can be
one by hierarchically decomposing the scene into a tree of
;
D
enclosing volumes [Clark 1976; Rubin and Whitted, 1980
adoun, Kirkpatrick and Walsh, 1982].
The ray-surface
t
e
intersection calculation proceeds by testing the outermos
nclosing volume ﬁrst and searches the subvolumes only if
c
the ray pierces that volume. Another approach is to form a
ellular decomposition of the scene, keeping track of which
s
a
surfaces are within or border a given cell.
If the ray i
ssumed to be in a particular cell then only the surfaces
-
p
inside that cell need be tested for intersections [see for exam
le Jones, 1971].
If no intersections are found, the ray
e
s
passes through that cell, enters a neighboring cell and th
earch continues.
Another technique, applicable if rays are
a
b
only traced to one level, is to enclose each surface with
ounding box in image space [Roth, 1982]. As the image is
w
scanned, surfaces become active or inactive depending on
hether the current raster location is within the surface’s
bounding box.
A different type of coherence results from the observa-
s
tion that in many scenes, groups of rays follow virtually the
ame path from the eye to the light source and thus can be
-
r
bundled into coherent beams of light (see Figure 1) [Han
ahan
and
Heckbert,
1984].
This
observation
can
be
l
r
exploited by attempting to trace a beam of rays in paralle
ather than an individual ray.
Exploiting this coherence is
.
A
advantageous since it reduces the number of intersections
lso, once such coherence is identiﬁed it allows incremental
r
techniques to be used for drawing a homogeneous region (a
egion over which the ray tree is constant).
This would
t
h
increase the speed of the rendering algorithm, especially a
igh resolutions.
Finally, it is just this lack of coherence
i
which causes many of the aliasing artifacts in ray traced
mages.
It is possible to use the coherence to antialias
s
Figure 1. A bundle of rays passing through two
pheres.
textures and shading calculations within a homogeneous
r
region and potentially, by identifying the boundaries between
egions, to antialias their edges.
We propose an algorithm for tracing beams through
g
p
scenes described by planar polygonal models. Beam tracin
olygons is much simpler because of the large body of
-
l
knowledge regarding both object space hidden surface calcu
ations and image space display algorithms. Also, unlike the
,
b
general case of a beam reﬂecting from a curved surface
eams formed at planar boundaries can be approximated by
n
p
pyramidal cones.
The algorithm we describe is similar i
rinciple to a technique developed by Dadoun, Kirkpatrick
s
t
and Walsh [1982] to trace sound beams from audio source
o a receiver. They noted that this problem is equivalent to
y
e
the hidden surface problem and proposed computationall
fﬁcient algorithms for performing rapid hidden surface
s
p
removal in static scenes. Our algorithm differs in that it i
atterned closely after the classic ray trace and creates output
2
that can directly drive image space rendering programs.
. Beam Tracing
The beam tracer is a recursive polygon hidden surface
a
algorithm. The hidden surface algorithm is designed to ﬁnd
ll visible polygons within an arbitrary two dimensional
t
region.
The procedure begins with the viewing pyramid as
he initial beam. The beam tracer builds an intermediate data
e
[
structure, the beam tree, which is very similar to the ray tre
Whitted, 1980].
Like the ray tree, whose links represent
r
rays of light and whose nodes represent the surfaces those
ays intersect, the beam tree has links which represent cones
t
of light and nodes which represent the surfaces intersected by
hose cones.
But unlike a link in a ray tree which always
t
m
terminates on a single surface, the beam link may intersec
any surfaces. Each node under the beam represents a visi-
i
ble surface element as seen along the beam axis. This is
llustrated abstractly in ﬁgure 2 and a simple example is
t
s
shown in ﬁgure 3.
The beam tree is computed in objec
pace and then passed to a polygon renderer for scan conver-
2
sion to form the ﬁnal shaded image.
.1. Object Space Beam Tracing
l
o
The beam tree could be formed using any of severa
bject
space
hidden
surface
algorithms.
The
pictures
t
Figure 2. A schematic representation of the beam
ree. Notice that the incident beam is fragmented
e
t
into several pieces each of which may give ris
o a reﬂected and refracted beam.
e
l
generated for this paper used an algorithm modeled along th
ines of the classic ray tracing program. We now outline the
procedure:
hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
b
function Beam-Trace( Beam : Polygon; Ctm : Matrix ) : PolygonList;
egin
{Transform scene into the beam coordinate system
{
using the current transformation matrix (Ctm)}
Priority sort all polygons in the scene (ScenePolygonList)}
For each Polygon in the ScenePolygonList do begin
P = Intersection( Beam, Polygon );
if P
≠ nil then begin
if RecurseFurthur( Depth, P, ... ) then begin
if reﬂective(Polygon) then begin
NewCtm = Ctm * ReﬂectionMatrix( Polygon );
e
P.ReﬂectiveTree = Beam-Trace( P, NewCtm );
nd
if refractive(Polygon) then begin
;
P
NewCtm = Ctm * RefractionMatrix( Polygon )
.RefractiveTree = Beam-Trace( P, NewCtm );
e
end
nd
{
end
Add P to the Beam polygon intersection list (FragmentList)}
e
Beam = Difference( Beam, Polygon );
nd
Beam-Trace = FragmentList;
h
end
hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
w
Most ray tracers perform all their calculations in the
orld coordinate system. The beam tracer performs all calcu-
-
i
lations in a transformed coordinate system, initially the view
ng coordinate system, called the beam coordinate system. In
e
s
the beam coordinate system, beams are deﬁned as the volum
wept out as a two-dimensional polygon in the x-y plane is
w
translated along the z-axis.
Since the transformation into
orld space may contain perspective, the most general beam
s
r
is a polygonal cone in world space. In a ray tracer, the ray i
edirected after a reﬂection or refraction whereas in the beam
-
t
tracer the scene is transformed into the beam coordinate sys
em.
This technique is analogous to forming the virtual
image of an optical system.
The closest beam-surface intersection is determined by
-
s
searching a depth-sorted list of polygons using two dimen
ional set operators.
The depth ordered list is formed by
,
1
priority sorting the polygons [Newell, Newell, and Sancha
972].
In our implementation, intersecting polygons and
a
t
cyclic dependencies are not allowed although this is not
heoretical limitation of the approach.
Such sorting is not
t
s
required during a ray trace but is characteristic of objec
pace hidden surface algorithms [Sutherland, Sproull, and
Schumacker, 1974] and implies that the worst case running
Figure 3. A beam tree corresponding to a cube
e
b
on a mirrored surface. The bold lines indicate th
eam fragments at that level in the tree.
time could be O (n ), which is worse than a ray trace which
2
is O (n ). Since we must depth-sort the polygons after every
o
t
beam intersection, algorithms that preprocess the scene s
hat priority ordering can be quickly determined from any
d
S
viewpoint could be used here [Sutherland, Sproull, an
chumacker, 1974; Fuchs, Kedem, and Naylor, 1980].
m
w
To ﬁnd the ﬁrst visible polygon, we intersect the bea
ith the ﬁrst polygon in the list. If the result is nil then the
s
a
polygon is outside the beam, otherwise it is visible and i
dded to the list of visible surface elements within this beam.
t
To ensure that no other polygon is classiﬁed as visible within
he area of this visible polygon, we subtract it from the beam
t
o
before continuing through the depth ordered list.
The se
perators used in this algorithm must be able to handle con-
-
f
cave polygons containing holes. Different methods for per
orming polygonal spatial set operations (union, intersection,
W
and difference) are discussed in [Eastman and Yessios, 1972;
eiler and Atherton, 1977].
To simulate reﬂection and refraction we call the beam
-
s
tracer recursively by generating new beams whose cross
ections are the intersection polygon.
The transformations
t
s
for reﬂection and refraction are discussed further in the nex
ection and the appendix.
Recursion of the beam tracer can be terminated by
several criteria:
(1)
Maximum tree depth: 5 levels is common.
y
(2)
Threshold of insigniﬁcance: determine if the intensit
contributed by this branch of the tree will make a
c
perceptible difference (called ‘‘adaptive tree depth
ontrol’’ in [Hall and Greenberg, 1983]).
w
(3)
Polygon size: terminate when polygon area is belo
some threshold, such as one pixel.
2.2. Reﬂection and Refraction Transformations
Reﬂection in a plane, which maps each point to its mir-
b
ror image, is a linear transformation, and can be represented
y a 4x4 homogeneous matrix. Refraction by a plane, how-
5
ever, is not a linear transformation in general. Figures 4 and
, which were made with a standard ray tracer, show the dis-
,
a
tortion of an underwater checkerboard viewed from above
nd an above-water checkerboard viewed from underwater,
respectively.
The second shows the effect of the critical
Figure 4. View of an underwater checkerboard
from air.
angle which occurs when a ray is refracted from the denser
l
a
material into the sparser one.
Rays incident at the critica
ngle are refracted parallel to the surface (toward the horizon
e
i
in our ‘‘ﬁsh-eye’’ view).
Outside the circle, when th
ncident angle is greater than the critical angle, there is no
o
n
refraction, and one has total internal reﬂection. (Humans d
ot see this phenomenon when swimming because our eyes
are not adapted to focus underwater [Walker, 1984]).
Since
refraction
bends
lines,
it
cannot
always
be
-
t
expressed as a linear transformation.
There are two situa
ions under which it is linear, however.
For orthographic
s
e
projections the incident angle is constant, and refraction i
quivalent to a skew or shear. The other situation is for rays
g
at near-perpendicular incidence, known as paraxial rays in
eometrical optics.
The latter corresponds to the centers of
.
D
ﬁgures 4 and 5, where lines are approximately linear
erivations of the matrix formulas for these transformations
are given in the appendix.
Since beam tracing, as outlined here, is limited to linear
i
transformations, we must choose one of these approximations
n order to simulate refraction.
The consequences are that
t
b
beam traced perspective pictures exhibiting refraction will no
e optically correct. There will be no critical angle, and lines
ﬁ
will never become bent.
Beam traced approximations to
gures 4 and 5 would look like normal checkerboards. The
d
error of the approximation is highest for refraction from
ense materials to sparse ones, but fortunately for beam trac-
,
l
ing, humans are normally on the sparser side (e.g. in air
ooking into water). This explains why refraction’s curvature
2
of lines is not widely known.
.3. Image Space Rendering
Associated with each screen-space polygon is the tree of
r
face fragments which are projected onto that polygon by the
eﬂection and refraction transformations. Final intensities can
a
g
be computed by scan converting all of the faces covering
iven area in parallel, and applying the recursive intensity
formula
I = c I +c I +c I +c It
t
d d
s s
r r
t
o the tree of faces.
This blends the diffuse, specular,
c
reﬂected,
and
transmitted
intensities
according
to
the
oefﬁcients c , c , c , and c
to compute a color for each
pixel.
d
s
r
t
Faceted or Gouraud shading of beam traced scenes can
be done using a modiﬁed painter’s algorithm. This was the
Figure 5. View of an above-water checkerboard
t
from underwater.
echnique used to produce the beam traced pictures in this
r
o
paper (ﬁgures 6-9). Polygons are drawn into a frame buffe
ne-by-one as the beam tree is traversed. For each face or
-
vertex, the diffuse and specular intensities I
and I
are com
d
s
,
1
puted using the Phong lighting model [Newman and Sproull
979]. Since the polygons in the fragment lists of the beam
e
t
tree are deﬁned in the beam coordinate system, they must b
ransformed back to ‘‘world space’’ for all shading calcula-
s
tions; shading should not be done in a virtual or perspective
pace. If the coefﬁcients c , c , c , c , and the material tran-
s
d
s
r
t
parency are constant over each polygon, rendering can be
s
e
done by simply adding the intensities into a frame buffer a
ach polygon is drawn. Since addition is commutative, the
r
beam tree can be traversed in any order.
This method
equires a fairly robust polygon tiler, since the polygons are
r
o
potentially concave with holes, and a single-pixel gap o
verlap between polygons will result in an edge which is too
s
l
dark or too light [Heckbert, 1983].
If the polygon tiler i
imited to convex polygons, concavities and holes can be
p
eliminated by polygon subdivision.
Note that the pixel to
ixel coherence allows antialiased texture mapping (ﬁgure 8).
m
Figure 6. A diffusely shaded cube resting on a
irror.
Figure 7. A translucent dodecahedron within a
cube of mirrors.
Figure 8. A reﬂective cube in the interior of a
F
texture mapped, reﬂective cube.
igure 9. An example of post-processing a beam
3
tree to simulate an Omnimax projection system.
. Discussion
A useful measure of any ray traced image is average ray
-
p
tree size. This statistic is similar to the notion of depth com
lexity used in analyzing polygon display algorithms. With
the beam tree this is easily calculated since
screen area
total area (in screen space) of the beam tree polygons
hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh
average ray tree size =
The total time spent ray tracing an image is equal to the
-
m
average ray tree size, multiplied by the time needed to deter
ine the nearest ray-polygon intersection, times the resolu-
i
tion of the image. In contrast, the beam tracer is resolution
ndependent and therefore the relative efﬁciency of beam
tracing versus ray tracing increases linearly with resolution.
Beam tracing, however, is more complicated than ray
e
e
tracing and may not always be worth the extra expense. Th
xpected improvement depends more on the intrinsic coher-
d
ence of the imaged scene rather than resolution.
We can
eﬁne coherence as
coherence =
total beam tree size
average ray tree size
hhhhhhhhhhhhhhhhhh
s
f
This
measure
indicates
to
what
degree
the
beam
i
ragmented—the more it is fragmented the lower the scene’s
b
coherence. If the coherence is very high then many rays are
eing traced in parallel and therefore a beam tracer will be
t
e
faster than a ray tracer. In summary, beam tracing is mos
fﬁcient when there are large homogeneous regions in the
picture.
Figures 6 and 7 took 30 seconds and 5 minutes, respec-
a
tively, to generate on a VAX 11/780 computer. We estimate
standard ray tracer would take 20-100 times longer.
a
r
One nice feature of the beam tracer is that it generates
easonably compact object space representation of the image.
d
Because it produces object space coordinates, precise line
rawings like ﬁgure 3 can be made.
These are difﬁcult to
e
r
make with a ray tracer [Roth, 1982].
The intermediat
epresentation can also be further manipulated. For example,
s
the same tree can be rendered at different resolutions or a
eries of images can be produced which differ in their color-
t
ing and lighting parameters.
Since the output of the beam
racer
is
in
object
space,
ﬁsheye
projections
such
as
v
Omnimax can be made by beam tracing in perspective with a
ery wide camera angle and distorting the result (ﬁgure 9).
p
Methods for mapping and subdividing lines for the Omnimax
rojection are given in [Max, 1983].
o
t
There are, however, some rather severe limitations t
he technique as we’ve developed it here. We exploit coher-
l
i
ence by assuming that reﬂections and refractions form virtua
mages within a polygonal window and that these virtual
s
images can be formed by linear transformations. As we’ve
hown, refraction under perspective is not a linear transfor-
-
t
mation, thus the pictures are not physically correct. In prac
ice it is very difﬁcult for ‘‘non-experts’’ to detect the
a
discrepancy in the pictures we’ve generated. This linearity
ssumption is also not correct for curved surfaces since their
normals vary from point to point.
We now discuss several possibilities for future exten-
3
sions of beam tracing.
.1. Light Beam Tracing
In standard ray tracing, diffuse shading of surface points
d
is done by aiming rays toward each point light source and
etermining if any objects are blocking the path of direct
illumination.
Blocked
lights
create
shadows;
unblocked
lights contribute to the reﬂected intensity using Lambert’s
l
d
law. Since rays are aimed only at the lights, and not in al
irections, this does not realistically model indirect illumina-
a
tion.
It is also intellectually unappealing because it creates
n asymmetry between the light source and the eye point,
contrary to the laws of physics.
Diffuse shading and shadows for a beam-traced model
.
W
can be computed as a pre-process to the polygon database
e propose a recursive extension of the Atherton-Weiler sha-
t
s
dow algorithm [1978].
Beams are traced from each ligh
ource just as they are from the viewpoint (‘‘light beam’’
r
i
tracing as opposed to ‘‘eye beam’’ tracing). The ﬁrst orde
ntersections are the directly illuminated surfaces, (not sha-
-
f
dowed) and higher order intersections are illuminated sur
aces resulting from the reﬂected and refracted light sources,
i
an effect difﬁcult to achieve with a standard ray tracer. The
lluminated polygons thus formed can be built into the model
t
a
database as ‘‘surface detail’’, that is, polygons which do no
ffect the shape of the objects, but only their shading. If the
-
s
light sources are inﬁnitely distant, each face will have a con
tant diffuse intensity, which can be compactly saved in the
-
i
database on this pre-process pass, for use during the render
ng pass.
This algorithm has several other advantages over diffuse
l
a
shading during rendering.
Light sources can be directiona
nd have a polygonal cross-section. They need not be point
,
s
sources. The depth of the eye beam tree can also be reduced
ince light beam tracing propagates shading information
s
m
through several bounces (one could say that the light beam
eet the eye beams halfway).
Finally, if the model and
-
p
lights are stationary during an animation, light beam pre
rocessing need be done only once.
3.2. Antialiasing
In classical ray tracing, antialiasing is usually done by
r
s
adaptive subdivision of pixels near large intensity changes o
mall objects [Whitted, 1980; Roth, 1982].
The method
-
q
attempts to use heuristic criteria to probe the image fre
uently enough that small details will not be overlooked.
t
Depending on the criteria, it will sometimes subdivide too lit-
le, resulting in aliasing, or too much, in which case process-
ing time is wasted.
Before rendering, it is possible to subdivide the beam
n
a
tree into non-overlapping polygonal regions and form a
djacency graph which indicates which regions are neighbors.
Given this information, antialiasing edges is straightforward.
Since the beam tracer resolves all hidden-surface questions,
i
all that is needed is a polygon scan converter with a pixel
ntegrator. Pixel integration can be done by sub-sampling or
-
t
with analytic methods [Catmull, 1978]. This same informa
ion might be useful when light beam tracing. The symmetry
d
p
between eye and light allows us to relate partially-covere
ixels
to
partially-obscured
lights:
the
former
suggests
,
i
antialiasing, the latter suggests soft shadows. Consequently
f a region adjacency graph is made during light beam trac-
3
ing, this can assist in the creation of soft-edged shadows.
.3. Rendering Options
There are many other interesting variations to rendering
e
o
the beam tree. If, as mentioned in the previous section, th
utput is divided into non-overlapping regions and we are
e
c
doing faceted shading, the recursive shading formula can b
alculated once for the entire region and a single polygon
g
t
rendered. At the other extreme, it might be worth modifyin
he polygon renderer so that it simultaneously tiles all the
f
l
polygons in a region. This would allow the simulation o
ight scattering through translucent materials, since in this
-
n
case the intensity is an exponential function of material thick
ess [Kay and Greenberg, 1979] which varies over the
e
n
polygons.
As we’ve mentioned, because of the additiv
ature of the shading formula, the polygons can be rendered
g
o
in any order but the overall intensity is modulated dependin
n the shading coefﬁcients and the depth in the tree. If, on
t
o
the other hand, we always render the scene in back-to-fron
rder, one can accommodate spatially varying reﬂection and
-
l
refraction coefﬁcients. For example, cut glass could be simu
ated by modulating these parameters with a texture map.
a
Finally, the coherence in the beam tree often allows certain
spects of the shading calculation to be done once per region
.
F
which allows a more complicated shading model to be used
or example, we have used constant values for the four
o
intensity coefﬁcients, but more realistic results could be
btained using Fresnel’s equations [Longhurst, 1967].
4. Acknowledgements
We would like to thank to Kevin Hunter and Jules
-
t
Bloomenthal for proofreading and Jane Nisselson for assis
ance with the writing.
5.
Appendix: Reﬂection
and Refraction
Transforma-
W
tions
e
derive the
homogeneous
4x4 matrix form for the
reﬂection and refraction transformations.
Figure 10. Geometry of reﬂection and refraction
F
in the plane of incidence.
igure 10 shows the geometry of an incident ray I hitting a
(
plane
and
generating
a
reﬂected
ray
R
and
refracted
transmitted) ray T . The three rays and the surface normal
N all lie in a plane. The index of refraction changes from η1
t
2
1
o η at the boundary. The angle of incidence is θ and the
angle of refraction is θ . We wish to ﬁnd the transformations
2
r
t
t
which map the real reﬂected and refracted points P
and P
o their virtual image P .
Notation:
P = (x y z 1)
= any point
LP = Ax+By+Cz+D = 0
L = (A B C D) = coefﬁcients of plane equation:
e N e =
A +B +C
= 1
N = (A B C 0)
= normal to plane
I = incident ray direction,
e I e =1
T
2
√
T
2
2
dddddddd
The direction of the reﬂected ray is given by:
where: d = cosθ = −N .I > 0
R = I −2(N .I )N = I +2dN
(a unit vector)
1
r
e
d
The reﬂected point can be found by noting that LP gives th
istance of a point P
r from the plane. The point formula has
:
a form similar to the direction formula
P = P −2(LP )N = P −2NLP = M Pr
w
r
r
r
r
r
r
here M
is the homogeneous 4x4 matrix for the reﬂection
transformation:
= I −2NL
=
I
J
J
J
J
L
0
−2AC
−2AB
1−2A
0
−2BC
1−2B
−2AB
0
1−2C
−2BC
−2AC
1
−2CD
−2BD
−2AD M
J
J
J
J
O
a
M r
2
2
2
nd I is a 4x4 identity matrix.
:
Snell’s law relates the incident and refracted angles
η sinθ = η sinθ2
1
1
2
:
The direction of the refracted ray is
and
c = cosθ =
1−η (1−d )
where: η = η /η = relative index of refraction
T = ηI −(c −ηd )N
(a unit vector)
1
2
2
2
2
√ddddddddd
f
1
There
is
no
refracted
ray
(total
internal
reﬂection)
i
−η (1−d ) < 0.
Our formula for T is equivalent to, but
s
2
2
impler than, the one in [Whitted, 1980].
s
i
For orthographic projections, the incident direction I
i
ndependent of object position, and refraction is a skew
transformation parallel to the plane which maps a point P as
t
follows:
M = vector tangent to plane,
e M e = sinθ
where: M = N ×(I ×N ) = I −(N .I )N = I +dN ,
P = P + (tanθ −tanθ ) MˆLPt
t
1
2
1
a
t
t
nd P = M P , where M t
is a 4x4 matrix:
M
= I + α(I +dN )L
t
1
1
2
1
2
h
hη
hh − c
1
hhhhhhhhhhh = secθ −ηsecθ = d
θ
tanθ −tan
θ
I
where: α =
sin
f viewing along the Z axis, then I = (0 0 1 0)
, d = −C ,
and
T
=
I
J
J
J
J
L
0
αA (1−C )
−αABC
1−αA C
0
αB (1−C )
1−αB C
−αABC
0
1+αC (1−C )
−αBC
−αAC
1
αD (1−C )
−αBCD
−αACD
M
J
J
J
J
O
2
2
2
2
2
2
2
2
T
M t
his formula is exact for orthographic projections. If the eye
e
s
is local, however, I and d vary from point to point on th
urface, and there is no linear transformation from P to P .
t
l
i
Observing ﬁgure 11, however, we see that rays with smal
ncidence angles (paraxial rays) produce virtual refracted rays
which nearly come to a focus. Thus, for paraxial rays,
D
D
hhhh = tanθ
tanθ
hhhhh = tanφ
tanφ
hhhhh = constant
2
1
1
2
1
2
1
2
n
If we take the constant to be η /η , the
η tanθ = η tanθ2
W
1
1
2
e
call
this
the
tangent
law.
For
paraxial
rays,
t
sinθ ∼∼ tanθ ∼∼ θ, so Snell’s law is in agreement with the
angent law.
A graphical comparison of the two laws is
t
Figure 11. Refracted paraxial rays come to a vir-
ual focus in the ﬁrst medium at a distance
-
D
= D /η from the plane.
Left diagram illus
2
1
trates tangent law, right illustrates Snell’s law.
T
shown in ﬁgure 12.
he virtual focus can be interpreted as follows: When look-
,
o
ing across a boundary with relative index of refraction η
bjects appear to be at η times their actual distance [Feyn-
-
a
man, 1963]. Recall that light travels slower in denser materi
ls by precisely this factor η. Within the paraxial approxi-
-
t
mation, then, refraction is equivalent to a scaling transforma
ion perpendicular to the plane:
P = P +(η−1)(LP )N = M Pt
F
t
t
t
igure 12. Refraction angle as a function of in-
.
F
cidence angle using Snell’s law and tangent law
or this graph, η /η = 1.33.
2
1
t
2
2
2
w
M
here: λ = η−1
= I +λNL
=
I
J
J
J
J
L
0
λAC
λAB
1+λA
0
λBC
1+λB
λAB
0
1+λC
λBC
λAC
1
λCD
λBD
λAD M
J
J
J
J
O
.
R
Note the similarity between this and the reﬂection formula
eﬂection is simply paraxial refraction with η = −1.
A
6. References
therton, Peter R., Kevin Weiler, and Donald Greenberg,
G
‘‘Polygon Shadow Generation.’’ Computer Graphics (SIG-
RAPH ’78 Proceedings), vol. 12, no. 3, Aug. 1978, pp.
C
275-281.
atmull, Edwin, ‘‘A Hidden-Surface Algorithm with Anti-
-
i
Aliasing.’’ Computer Graphics (SIGGRAPH ’78 Proceed
ngs), vol. 12, no. 3, Aug. 1978, pp. 6-11.
e
S
Clark, James, ‘‘Hierarchical Geometric Models for Visibl
urface Algorithms.’’ C.A.C.M.
vol. 19, no. 10, 1976, pp.
C
547-554.
row, Franklin C., ‘‘Shaded Computer Graphics in the Enter-
.
1
tainment Industry.’’ Computer, vol. 11, no. 3, March 1978, p
1.
Dadoun, Norm, David G. Kirkpatrick, and John P. Walsh,
T
‘‘Hierarchical Approaches to Hidden Surface Intersection
esting.’’ Proceedings of Graphics Interface ’82, May 1982,
E
pp. 49-56.
astman, C. M., and C. I. Yessios, ‘‘An Efﬁcient Algorithm
l
D
for Finding the Union, Intersection and Differences of Spatia
omains.’’ Technical Report 31, Institute of Physical Plan-
F
ning, Carnegie-Mellon University, Sept. 1972.
eynman, Richard P., Robert B. Leighton, and Matthew
,
R
Sands, The Feynman Lectures on Physics. Addison-Wesley
eading, Mass., 1963, vol. I, pp. 27-3, 27-4.
n
V
Fuchs, Henry, Zvi M. Kedem, and Bruce F. Naylor, ‘‘O
isible Surface Generation by A Priori Tree Structures.’’
,
n
Computer Graphics (SIGGRAPH ’80 Proceedings), vol. 14
o. 3, July 1980, pp. 124-133.
Hall, Roy A., and Donald P. Greenberg, ‘‘A Testbed for
A
Realistic Image Synthesis.’’ IEEE Computer Graphics and
pplications, vol. 3, no. 8, Nov. 1983, pp. 10-20.
m
T
Hanrahan, Pat, and Paul S. Heckbert, ‘‘Introduction to Bea
racing.’’ Proc. Intl. Conf. on Engineering and Computer
Graphics, Beijing, China, Aug. 1984.
Heckbert, Paul, PMAT and POLY User’s Manual. New York
J
Inst. of Tech. internal document, Feb. 1983.
ones, C. B., ‘‘A New Approach to the ‘Hidden Line’ Prob-
.
2
lem.’’ The Computer Journal, vol. 14, no. 3, Aug. 1971, pp
32-237.
Kawaguchi, Yoichiro, ‘‘Growth: Mysterious Galaxy.’’ SIG-
K
GRAPH ’83 Film & Video Shows, p. 5.
ay, Douglas S., and Donald Greenberg, ‘‘Transparency for
-
G
Computer Synthesized Images.’’ Computer Graphics (SIG
RAPH ’79 Proceedings), vol. 13, no. 2, Aug. 1979, pp.
L
158-164.
onghurst, R. S., Geometrical and Physical Optics.
Long-
M
man, London, 1967.
ax, Nelson, ‘‘Computer Graphics Distortion for IMAX and
.
1
OMNIMAX Projection.’’ Nicograph ’83 Proceedings, Dec
983, pp. 137-159.
Newell, M. E., R. G. Newell, and T. L. Sancha, ‘‘A New
.
C
Approach to the Shaded Picture Problem.’’ Proc. ACM Nat
onf., 1972, p. 443.
Newman, William M., and Robert F. Sproull, Principles of
Y
Interactive Computer Graphics, 2nd ed. McGraw-Hill, New
ork, 1979.
Nishimura,
Hitoshi,
Hiroshi
Ohno,
Toru
Kawata,
Isao
-
l
Shirakawa, and Koichi Omura, ‘‘Links-1: A Parallel Pipe
ined Multimicrocomputer System for Image Creation.’’ IEEE
-
p
1983 Conf. Proc. of the 10th Annual Intl.
Symp. on Com
uter Architecture.
Roth, Scott D., ‘‘Ray Casting for Modeling Solids.’’ Com-
.
1
puter Graphics and Image Processing, vol. 18, no. 2, Feb
982, pp. 109-144.
Rubin,
S.
M.,
and
Turner Whitted,
‘‘A
3-Dimensional
C
Representation for Fast Rendering of Complex Scenes.’’
omputer Graphics (SIGGRAPH ’80 Proceedings), vol. 14,
S
no. 3, July 1980, pp. 110-116.
utherland, Ivan E., Robert F. Sproull, and Robert A.
A
Schumacker, ‘‘A Characterization of Ten Hidden-Surface
lgorithms.’’ Computing Surveys, vol. 6, no. 1, March 1974,
W
p. 1.
alker, Jearl, ‘‘The Amateur Scientist: What is a ﬁsh’s view
’
S
of a ﬁsherman and the ﬂy he has cast on the water?’
cientiﬁc American, vol. 250, no. 3, March 1984, pp. 138-
143.
Walsh, John P., and Norm Dadoun, ‘‘What Are We Waiting
d
m
for? The Development of Godot, II.’’ presented at the 103r
eeting of the Acoustical Society of America, Chicago, April
W
1982.
eiler, Kevin, and Peter Atherton, ‘‘Hidden Surface Remo-
-
G
val Using Polygon Area Sorting.’’ Computer Graphics (SIG
RAPH ’77 Proceedings), vol. 11, no. 2, Summer 1977, pp.
W
214-222.
hitted, Turner, ‘‘An Improved Illumination Model for
.
3
Shaded Display.’’ C.A.C.M.
vol. 23, no. 6, June 1980, pp
43-349.
View publication stats
View publication stats
