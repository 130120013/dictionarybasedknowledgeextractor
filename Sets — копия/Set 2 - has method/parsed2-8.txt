Applied Acoustics 38 (1993) 145-159 
Binaural Room Simulation Based on an Image Source 
Model with Addition of Statistical Methods to Include the 
Diffuse Sound Scattering of Walls and to Predict the 
Reverberant Tail 
Renate Heinz 
Department of Technical Acoustics, Technical University of Aachen, 
Templergraben 55, W-5100 Aachen, Germany 
(Received 26 November 1991; revised version received 11 February 1992; 
accepted 14 February 1992) 
A BS TRA C T 
In order to predict the aural impression of the listening conditions which is to 
be expected at the various seats of a room (e.g. a concert hall) in particular 
when this is still in the stage of design, binaural room impulse responses for 
given source and receiver positions must be determined. This paper describes 
an approach to add a diffuse background signal to the result of an image 
source method, in which only the specular parts of the reflections are 
considered. The missing diffusely scattered energies are handled by a method 
based on the result of a sound particle tracing procedure with a low time 
resolution of a few milliseconds. This combined method is used to predict the 
direct sound and the early reflections of the binaural room impulse response. 
The reverberant part and the diffuse background signal are determined with 
methods which require only little room-specific information and obtain the 
remaining data statistically. For the late part of the binaural response a 
simplified simulation method is presented, that is based on the frequency- 
dependent reverberation time. Furthermore, the paper discusses the 
connection of two parts of a room impulse response, which are calculated by 
means of different methods. 
1 INTRODUCTION 
Binaural room simulation has primarily been based on the image source 
model and variations of it. Among other works, Allen and Berkley 1 and 
145 
Applied Acoustics 0003-682X/93/$06.00 © 1993 Elsevier Science Publishers Ltd, England. 
Printed in Great Britain 
146 
Renate Heinz 
P6sselt 2 made use of a straight image source calculation, that was applied on 
rectangular rooms. In this simple case the pattern of mirror images is known 
and there is no need for an expensive visibility check of the image sources for 
each listener position. By using very efficient variants of the image source 
method, it was then possible to also simulate complicated room shapes in a 
fairly short computation time. Examples for this are the cone-method 3 or 
the combined ray-tracing/image-source algorithm, 4 where a ray tracing is 
used to predict the image sources contributing to the response at the receiver 
('visible images'). References 5-8 give some advice on the binaural 
simulation based on these algorithms. 
However, the image source model takes only specular wall reflections into 
account. Diffuse sound scattering from structured surfaces is totally 
neglected. The sound particle method, 9 which is successfully used to predict 
the objective room acoustical parameters, actually can include non-specular 
components from rough surfaces to some extent through the assumption 
that the surfaces reflect the sound in a partially diffuse way. For that, a 
method is described in Ref. 10, that applies the sound particle model to the 
prediction of a binaural room impulse response. This method is called 'high- 
resolution sound particle method' since the time resolution should be much 
higher than for the prediction of room acoustical parameters only. To get a 
realistic spatial impression of the room, not only the features of the hall, but 
also the individual head related transfer functions of the listener have been 
considered. By convolving the predicted binaural response with an 
anechoically recorded signal (e.g. dry music or speech) the room response 
to this signal can be calculated. Using headphones or freefield- 
presentation, 11'12 the sound impression of the synthetic room can be 
examined. The quality of the simulation method used can be checked by 
comparing the results, which are predicted for already existing rooms with 
the corresponding measured data. Preliminary listening tests showed the 
importance of the diffuse wall reflections. 
Although the results of the high-resolution sound particle method sound 
surprisingly good, a certain unreliability in assigning data occurs, that 
question this approach. Moreover, this method needs to trace quite a lot of 
sound particles to get a high-resolution result of sufficient accuracy, which 
makes it very time intensive. However, the diffuse reflections do not have to 
be known as accurately as the specular reflections. Therefore, in section 2 of 
this paper a possibility is proposed to add a diffuse background signal to a 
slightly modified image source approach. This method combines the 
advantages of the image source model with those of the sound particle 
tracing technique. 
Binaural room simulation 
147 
2 AN IMAGE SOURCE METHOD WITH ADDED DIFFUSE 
REFLECTIONS 
The basis for the binaural simulation of the specular reflected parts is the 
combined ray-tracing/image-source algorithm 4 (hybrid method). In 
addition to that, the diffuse parts are synthesised by means of a pseudo- 
statistical method using data of the sound particle tracing program 
described in Ref. 13. Of the two methods for modelling the absorption at the 
surfaces, the energy reduction algorithm is chosen over the annihilation' 
algorithm since this method has the advantage that the uncertainty in 
energies does not increase with time. The same algorithm is used in the 
hybrid method to predict the visible mirror images. Some details of the 
methods used are described as follows. 
At first, the geometrical and acoustical input data of the considered hall 
are required. Curved surfaces must be modelled by means of planes, which 
are limited by polygons. The positions o fall corners of these polygons as well 
as the positions of the sound sources and the receivers, have to be defined in a 
given coordinate system. Additionally, the frequency-dependent sound 
absorption and diffusion coefficients of every surface and the directivity 
pattern of the sources must be known. Both methods are based on the 
assumption, that the wall impedances are real and that the whole frequency 
range may be split up into several frequency bands in a way that within each 
band the features can be assumed nearly constant. For the loudspeakers 
frequency responses with finer resolution are used. 
2.1 The prediction of the specular parts 
The hybrid method uses a sound particle tracing in order to get most of the 
audible image sources without the need of time intensive visibility checks. 
Therefore, a ray tracing process is performed in which a certain amount of 
sound particles is started simultaneously from every sound source. Time 
delays (e.g. when using loudspeakers) can also be simulated. The sound 
particles have a certain initial energy and are radiated in different directions 
which depend on the directivity of the sound source concerned. Every sound 
particle is traced until the maximum simulation time is reached or the 
particle energy falls below a defined minimum energy level. Whenever a 
sound particle hits a surface, the particle energy is reduced by (1 - a)(1 - d), 
where a denotes the absorption and d the diffusion coefficient of the surface 
in the considered frequency range. Since only specular reflections at the 
surfaces are considered one ray-tracing process is sufficient, but there must 
148 
Renate Heinz 
be an array of energies stored for all the different frequency ranges. The 
energies and the arrival times of the sound particles are registered whenever 
they pass through a receiver sphere. The radius of these spheres is set to 1 m. 
Furthermore, a list is stored including the sequence of indices of each 
surface from which a detected particle has been reflected during its path. 
Each particle represents a possible path from the considered sound source to 
the receiver position and thus yields a visible mirror image source. Since 
more than one particle may represent the same image source, a decision has 
to be made whether an image source has already been detected. Therefore, 
with every detection the index list will be compared with the entries of all 
valid image sources found before. Only new index series lead to new entries 
of image sources. With the aid of a tree-structure the compare routine can be 
realised with efficiency. Once all particles have been traced, the locations and 
the strength of all relevant mirror images are known in the given frequency 
bands. The energy of every image source is distributed on the surface of a 
sphere. Since this surface increases with the square of the distance r from the 
image source, the detected energies are reduced due to (l/r) -2. 
In addition to that, the directions of sound incidence are registered for 
each receiver position. For this purpose, a head-related coordinate system is 
established, in which the x-axis is the line of the listener's vision, the y-axis is 
the ear axis and the z-axis is the body axis. The direction of incidence, given 
by the azimuth angle 0 and the elevation angle ~, is then classified into 
defined directional groups. To this end, the upper hemisphere is divided into 
37 directional groups with an angular sampling step of 30 ° in the azimuth 
and elevation angle (see Fig. 1). All points of the lower hemisphere are 
allotted to the corresponding azimuth angles of the horizontal plane. As 
long as the audience is not elevated compared with the position of the sound 
source this lumping of reflections can be justified by the fact that the 
audience area is, in general, a highly absorbing surface, so that a sound 
incidence from below is very improbable. 
For each directional group the head-related impulse response is measured 
with subminiature microphones in the right and left ear channel of several 
persons under test with a maximum length sequence measuring system. 14 
An elastic holder fixes the position of the microphones about 4 to 5 mm 
inside the ear channels. 
After the tracing of all sound particles for each mirror image the energy, 
the distance and the directional group corresponding to the angle of 
incidence are stored. The energies of each frequency range are square-rooted 
and a term, exp(-0"5mr), is applied considering the attenuation due to the 
air absorption, where m is the air absorption coefficient and r is the actual 
distance. Then, every part will be convolved with the head-related impulse 
response of the right and left ear of a person under test according to the 
Binaural room simulation 
149 
90°~,-~. 
//I 
 ooi 
if. 
, 
• 
0o c./l.! 
90* 
Fig. 1. Classification of the angles of incidence. 
obtained directional group. The result of a summation of all these parts is a 
binaural impulse response, which is only valid in the frequency range for 
which the absorption and diffusion coefficients have been chosen. The 
following band pass filtering ensures that the impulse response is assigned to 
the correct frequency range. After the determination of all band pass filtered 
signals, usually in octave bands, they are added with correct phase and yield 
the wide-band binaural impulse response. In this context the correct phase 
summation means that there is no phase shift between the different band 
pass filter responses. 
According to the energy reduction term (1 -a)(1 -d) this result contains 
only the specular parts of the reflections. The missing diffusely scattered 
parts are predicted by a pseudo-statistical method which is based on a sound 
particle tracing result with a low time resolution. 
2.2 The prediction of the diffuse parts 
The developed pseudo-statistical method is based on the assumption that 
the parts due to the diffuse scattering on rough surfaces do not have to be 
known as precisely as the specular parts. Thus, the directional distribution of 
sound incidence may be assumed random and it should be sufficient to 
synthesise the spectral and temporal behaviour of the diffuse background 
signal as correctly as possible, but not each detail. 
The behaviour mentioned above can be estimated by means of a sound 
150 
Renate Heinz 
particle tracing procedure with a low time resolution. In contrast to the 
sound particle algorithm which is used to find the visible mirror images in 
the hybrid method described before, this approach only deals with the 
diffusely scattered parts. Whenever a sound particle hits a surface, the 
particle energy is reduced by 1-a, where a denotes the absorption 
coefficient of the surface in the considered frequency range. Furthermore, 
the comparison of a random number and the actual diffusion coefficient of 
the surface decides on whether the reflection is specular or random 
corresponding to Lambert's law. Since the diffusion coefficients can be 
frequency dependent and the diffuse wall reflections are handled by random 
ray reflections like those of a Monte Carlo procedure, a new ray-tracing 
process has to be started for each frequency range to be taken into account. 
Only if the last reflection was non-specular, is the particle energy detected 
when passing through a receiver sphere. The radius of the spheres is also set 
to 1 m. In order to consider the attenuation due to the air absorption a term 
exp(-mr) is applied on the energies. All detected energy parts are added in 
defined time intervals of a few milliseconds. The result is a histogram of 
statistical energy average values for every time interval and every considered 
frequency range. These distributions can be used to describe a time-variable 
filter whose characteristics match with the power spectrum of the diffuse 
sound energy and its temporal changes. This power spectrum is imposed on 
a Poisson process. For that a sequence of Dirac pulses which are Poisson 
distributed along the time axis is generated. Of course, the temporal density 
of these pulses must be high enough to ensure that the sequence will be 
perceived as continuous noise. The filtering of the Dirac pulses with the time- 
variable filter is made in the time domain, namely by convolving each pulse 
of the Poisson sequence with the inverse Fourier transform of the square- 
rooted, smoothed and interpolated power spectrum of the corresponding 
time interval. Moreover, every pulse is convolved with a head-related 
transfer function corresponding to a randomly distributed directional 
Fig. 2. Example of a diffuse background, 
Binaural room simulation 
151 
group. Since the dimensions of the directional groups are not identical, 
weighting factors must be implemented. 
An example for the resulting wide-band signal is shown in Fig. 2. The 
addition of the predicted diffuse signal with the specular part obtained by 
means of the image source method yields the complete binaural room 
impulse response. This addition is done with regard to a normalisation 
factor which makes sure that the direct sound would be of the same 
magnitude for both simulation techniques. From now on the combination 
of the two methods dealing with the specular and non-specular reflections is 
called the 'combined method'. 
3 FIRST RESULTS OF PRELIMINARY LISTENING TESTS 
In order to examine the influence of the diffuse reflections and the quality of 
the predicted results, the binaural impulse response is measured at different 
listener positions in an existing room employing the subminiature 
microphone techniques as already mentioned. Then the same acoustical 
environment is simulated with the combined method. To compare the 
simulated signals with the measured ones, the directivity and the spectral 
properties of the loudspeaker used for the measurements have to be taken 
into account. 
The comparison of the measured and simulated responses is made for the 
multi-purpose hall shown in Fig. 3. It has a volume of about 14 000 m 3 and a 
total surface of approx. 4000m 2. Important scattering surfaces are the 
ceiling and the audience area which is covered with slightly upholstered 
seats, but not occupied. In the computer model this region is represented by 
an area of homogeneous absorption which is situated on the floor of the hall. 
The absorption coefficients of the walls are measured with the in-situ 
measuring system described in Ref. 15. This method yields good results for 
nearly even surfaces, but the uncertainties grow with the wall diffusion. 
Therefore, the absorption measurements of the seats are made in a 
reverberant room. The diffusion coefficients are estimated using values of 
simple structures obtained by measurements or from the literature. 
The measurements of the binaural impulse responses are carried out with 
a nearly omnidirectional sound source. The same conditions are assumed for 
the simulations. According to the maximum time where the simulation is 
terminated, the measured signals are also restricted to 0"5 s. The purpose of 
the first preliminary listening tests was to judge the influence of the diffuse 
scattering on the aural impressions. Three different signals are compared: 
the results of the combined method with use of the estimated diffusion 
coefficients of the surfaces, the results of the hybrid method without any 
152 
Renate Heinz 
Fig. 3. 
a) 
b) 
e) 
[a) The multi-purpose hall in a three-dimensional view; (b) top view of the hall, 
S--source position, L--listening poisition; and (c) side view of the hall. 
diffuse wall scattering (diffusion coefficients are set to zero) and the measured 
signals. The most critical case is the test of the measured and simulated 
binaural room impulse responses themselves. Moreover, these signals are 
convolved with dry music or speech. The comparison of the room impulse 
responses shows that the non-diffuse simulation yields a somewhat 
synthetic, hard and crackly sound impression, whereas the results of the 
combined method sound softer and more like the measured signals. 
Several reasons may cause the differences between the measured signals 
and the responses synthesised by means of the combined method. One of them 
consists of he assumption of real wall impedances. This restriction is not a 
general problem of the image source model, but has only to do with the 
described band pass filtering approach. Furthermore, the manner of 
modelling the room shape prior to the simulation takes influence. Other 
possible causes may be the angular dependence of the wall absorption, the 
edge diffract!on, the attenuation due to the grazing propagation above the 
seats and the 'seat dip effect' which are not modelled in the simulation. 
The comparison of the convolved signals show audible differences between 
the measured and the non-diffuse signals, especially in the more critical case 
of speech transmission, whereas the combined method including the diffuse 
wall scattering leads to rather good results. From this experience it was 
concluded that the consideration of the diffuse wall reflections is important 
Binaural room simulation 
153 
and must not be neglected. This is in accordance with the results of the high- 
resolution sound particle method.l° 
4 SOME REMARKS ON ACCURACY AND COMPUTATION 
TIME 
The computation time of the sound particle method is proportional to the 
number of sound particles and the maximum simulation time. Since sound 
particle tracing is a statistical method the results are covered with 
uncertainties, which can be characterised by means of a relative standard 
deviation a~/(E) of the energies in the certain time intervals. According to 
Ref. 13 the total number of sound particles using the energy reduction 
algorithm may be estimated by 
V 
N- nr~c At(ae/(E)) 2 
where V is the volume of the considered hall, c the speed of sound, r k the 
radius of the receiver spheres and At the interval length. 
With a given radius r k and uncertainty ar./(E), the number of radiated 
particles increases in inverse proportion to the interval length At if this is 
made smaller and smaller. Fortunately, the integrating behaviour of the 
human ear does not require a great accuracy of the diffuse energies in very 
small intervals. The demand of sufficient accuracy of the averaged energies 
in time intervals of a few milliseconds allows the use of small particle 
numbers and therefore the computation time for each considered frequency 
range will be fairly short in contrast to the high-resolution sound particle 
method. 
Although the hybrid image source method needs only one ray-tracing 
process to yield the positions and the strength of all relevant mirror images 
for a given source and receiver position, the computation time of this 
method increases with the third power of the maximum simulation time, 
tma r In the example mentioned above the maximum time of 0-5 s is about a 
quarter of the averaged reverberation time of the considered hall. This 
simulation time however is not long enough to yield realistic aural 
impressions. Thus, the later part of the room impulse response must be 
predicted, too. Since on the one hand the computation time increases 
drastically with tin, x and on the other hand, according to our experience, the 
later part of the binaural room impulse response need not be determined 
with the same accuracy as the first part, the combined method is only used 
for the prediction of the early part of the binaural room impulse response. 
The later part can be synthesised with less time-intensive methods. In the 
next section some possibilities serving this purpose will be presented. 
154 
Renate Heinz 
5 SIMULATION OF THE REVERBERANT PART 
According to Junius 16 the diffusion of the sound field increases rapidly in the 
temporal response of a room. Therefore, the accurate knowledge of the 
directional distribution in the reverberant part is not needed, provided that 
the room is 'well-shaped' (the proportions should be balanced, that means 
flat or long rooms should not be considered). Instead of this, the directions of 
sound incidence of all reflections can be determined statistically from a 
certain time on. On this basis, a reverberant tail can be synthesised in such a 
way, that only its gross temporal and spectral behaviour agrees with that of 
the true decay process, whereas the complicated 'microscopic' structure of 
the latter is abandoned. In agreement to the pseudo-statistical approach for 
the diffuse parts described above this is done with a filtered Poisson process. 
The difference, however, is that here the filter characteristics agree with the 
power spectrum of the decaying sound energy and its temporal changes. 
One way to determine these characteristics 17 is to use sound particle 
tracing data of low time resolution (interval length a few milliseconds) for a 
couple of frequency bands. In contrast to the method described in section 2 
these data are obtained by considering all specular and non-specular energy 
parts in a certain time interval. The power spectra of each time interval are 
smoothed and interpolated after drawing the square-root and yield the 
magnitude of the filter characteristics. The phase, which is of no subjective 
significance, can be chosen in any convenient way (phase jumps excluded). 
This filtering procedure is also carried out in the time domain by convolving 
the Poisson sequence with the inverse Fourier transform ri(t) of the filter's 
transfer function. 
As an alternative, the functions ri(t) can be determined by inverse Fourier 
transformation of the spectra [R(f)]",, where n~ is the average number of 
wall reflections that a sound particle will undergo during a travelling time t~. 
R(f) represents the spectrum of the averaged reflection factors. It can be 
determined by averaging the octave-band absorption coefficients of all 
surfaces, which are also needed for the previous simulation of the early part 
of the room impulse response. Then the spectrum R(f) is obtained with 
a = 1 - R 2 by applying, smoothing and interpolation. The attenuation due 
to the air absorption is also included. 
Figure 4 shows a reverberant part, which is obtained by means of the latter 
method. The magnitude decreases exponentially with the time and the 
typical time dependence of the reverberation (higher frequencies are damped 
faster than the lower frequencies) is proved by preliminary listening tests. 
There may be a remark in place concerning the statistics of the employed 
Poisson process. Provided the density of pulses is high enough, it is perceived 
Binaural room simulation 
155 
0.5s 
2.5s 
Fig. 4. 
Statistical reverberant part. 
as white or coloured noise depending on the settings of the filter parameters. 
The same holds for its intensity. Therefore, it is sufficient to choose the pulse 
rate high enough whereas the increase of reflection density as occuring in 
real reverberation may be neglected. 
Since the second method needs less information and thus even less 
computation time, it is especially useful for the simulation of the late part of a 
binaural room impulse response. With regard to the calculation time it 
seems reasonable to use both methods. For that the simulation is split up in 
three steps. The direct sound and the early reflections are calculated by 
means of the combined method. The statistical method using the frequency- 
dependent reverberation times simulates the late reverberant tail and the 
method proposed in Ref. 17 is used to obtain the early reverberant part 
between these two areas. 
6 CONNECTION OF TWO PARTS OF A ROOM IMPULSE 
RESPONSE 
In this section some problems of connecting two parts of a room impulse 
response, which are calculated by means of different simulation methods, are 
discussed. For example, a result of the combined method is to be connected 
with a reverberant part, which is calculated by means of the statistical 
reverberation method described above. In order to avoid audible dis- 
turbances some rules relating the connection have to be obeyed. First of all, 
the two parts should be attached smoothly to one another. This condition can 
be realised by connecting the two parts in unidirectional zero transitions as 
shown in Fig. 5. In Fig. 5(b) the chosen zero transitions are marked with 
156 
Renate Heinz 
a) 
b) 
,) 
.
.
.
.
.
.
 
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
 
Fig. 5. 
Connection of two signals in unidirectional zero transitions. ( 
( ...... ) signal 2. 
-) Signal 1, 
vertical lines. Fig. 5(c) shows the smoothly connected signal. Moreover, there 
should be no great difference between the spectral features at the point of 
connection, because the human ear is very sensitive to such changes. 
Figure 6 presents a connected room impulse response and a zoomed 
section near the connection point. This response was generated by 
connecting results of a hybrid mirror image program to signals predicted by 
means of the high-resolution sound particle method. The simulation is based 
on the geometrical and acoustical data of a room with several highly 
scattering walls. Although the connection point is not visible, a distinct 
sound colouration can be heard in the binaural impulse response. 
Consequently the convolved signals are not very similar to the measured 
ones. This can be explained by the influence of the diffuse wall scattering on 
the spectra. 
The best way to avoid or to solve the connecting problems may be to use 
very similar methods for the synthesis of the different sections of the binaural 
Binaural room simulation 
157 
=) 
O.Os 
b) 
0.3s 
t 
[ connection point~ 
0.8S 
Fig. 6. 
8 
6 
4 
iA 
2  lvvA i 
0 , ,,~//i 
-2 
, 
VV 
-6 
I 
'V ~/|./J~/'vl I V v~ 
v 
iV 
w 
~ " V V 
,v 
296 
298 
300 
302 
ms 
f 
[ connection point] 
Example of a connected room impulse response, (a) the entire connected response, 
and (b) a zoomed section near the connection point. 
response or to provide an overlapping area or a range of assimilation 
between the predicted parts. For this subject, however, more experience is 
needed. 
7 CONCLUSIONS 
The comparison between results of a slightly modified image source method 
combined with a pseudo-statistical approach considering diffuse reflections 
and the results of pure image source method shows that the inclusion of 
the diffuse wall scattering is important. In order to reduce the computation 
time the combined method is only used to predict the direct sound and the 
early reflections. Solving the problem of a smooth connection, the binaural 
response can be completed by means of a reverberant part, which can be 
predicted in a fairly short computation time. 
The described methods will be tested for several listening positions in 
different halls. With a direct comparison of the measured and simulated 
158 
Renate Heinz 
signals the quality of the simulation can be checked. The first results are 
promising, but there are still some questions left unanswered. 
ACKNOWLEDGEMENTS 
The author wishes to thank Prof. H. Kuttruff for helpful discussions and 
useful comments on this manuscript. This work was supported by the 
Deutsche Forschungsgemeinshaft. 
REFERENCES 
1. Allen, J. B. & Berkley, D. A., Image method for efficiently simulating small- 
room acoustics. J. Acoust. Soc. Amer., 65 (1979) 943-50. 
2. P6sselt, C., Binaurale Raumsimulation ffir Kopfh6rerwiedergabe. In Fort- 
schritte der Akustik--DAGA '87. DPG-GmbH, Bad Honnef, Germany, 1987, S. 
725-8. 
3. Van Maercke, D., Simulation of sound fields in time and frequency domain 
using a ,geometrical model. In Proceedings of the 12th International Congress on 
Acoustics, Toronto, Canada, 1986, E11-7. 
4. Vorl~inder, M., Simulation of the transient and steady-state sound propagation 
in rooms using a new combined ray-tracing/image-source algorithm. J. Acoust. 
Soc. Amer., g6 (1989) 172-8. 
5. Martin, J. & Vian, J. P., Binaural sound simulation of concert halls by a beam 
tracing method. In Proceedings of the 13th International Congress on Acoustics. 
Belgrade, 1989, Sava Centar, Beograd, Yugoslavia, pp. 253-6. 
6. Kuttruff, H., Vorl~inder, M. & Cial3en, Th., Zur geh6rmfil~igen Beurteilung der 
'Akustik' von simulierten R~iumen. Acustica, 70 (1990) 23(~1. 
7. ClafSen, T. & Grundmann, R., H6rvergleiche in realen und computersimulierten 
Rfiumen. In Fortschritte der Akustik--DAGA '90, DPG-GmbH, Bad Honnef, 
Germany, 1990, S. 839-42. 
8. Lehnert, H., Erzeugung von virtuellen akustischen Umgebungen. Fortschritte 
der Akustik--DAGA '90, DPG-GmbH, Bad Honnef, 1990, S. 895-8. 
9. Krokstad, A., Strom, S. & Sorsdal, S., Calculating the acoustical room response 
by the use of a ray tracing technique. J. Sound Vib., 8 (1968) 118-25. 
10. Heinz, R., Ein hochaufgel6stes Schallteilchenverfahren zur binauralen 
Raumsimulation unter Berficksichtigung der diffusen Wandstreuung. Acustica, 
75 (1992) 246-55. 
11. Damaske, P. & Mallert, V., Ein Verfahren zur richtungstreuen Schailabbildung 
des oberen Halbraumes fiber zwei Lautsprecher. Acustica, 22 (1969/70) 153-62. 
12. Neu, G., Mommertz, E. & Schmitz, A., Untersuchungen zur richtungstreuen 
Schallwiedergabe bei Darbietung von kopfbezogenen Aufnahmen fiber zwei 
Lautsprecher. Acustica, 76 (1992) 183-92. 
13. Vorliinder, M., Untersuchungen zur Leistungsf~ihigkeit des raumakusticken 
Schallteilchenmodells. Dissertation, RWTH, Aachen, Germany, 1989. 
Binaural room simulation 
159 
14. Schmitz, A. & Vorlfinder, M., Messung von Aul3enohrstol3antworten mit 
Maximalfoglen-Hadamard-Transformation und deren Anwendung bei lnver- 
sionsversuchen. Acustica, 71 (1990) 257-68. 
15. Wilms, U. & Heinz, R., ln-situ-Messung komplexer Reflexionsfaktoren von 
Wandfl~ichen. Acustica, 75 (1991) 28-39. 
16. Junius, W., Raumakustische Untersuchungen mit neueren MeBverfahren in der 
Liederhalle Stuttgart. Acustica, 9 (1959) 289-303. 
17. Kuttruff, H., Zur H6rbamachung der Schallfibertragung in simulierten S/lien. 
Fortschritte der Akustik--DAGA '91, DPG-GmbH, Bad Honnef, Germany, 
1991, S. 625-8. 
