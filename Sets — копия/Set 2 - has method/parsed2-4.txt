See discussions, stats, and author profiles for this publication at: https://www.researchgate.net/publication/4104296
Algorithms for simulation of sound and ultrasound propagation in complex
dispersive environments
Conference Paper · July 2004
DOI: 10.1109/ELMAR.2004.1356389 · Source: IEEE Xplore
CITATION
1
READS
72
1 author:
Some of the authors of this publication are also working on these related projects:
Beam Tracing with Refraction View project
Acoustic Simulation Based on Hybrid Beam-Ray Tracing View project
Marjan Sikora
University of Split
27 PUBLICATIONS   152 CITATIONS   
SEE PROFILE
All content following this page was uploaded by Marjan Sikora on 21 May 2014.
The user has requested enhancement of the downloaded file.
ALGORITHMS FOR SIMULATION OF SOUND AND 
ULTRASOUND PROPAGATION IN COMPLEX DISPERSIVE 
ENVIRONMENTS 
 
Marjan Sikora 
 
Faculty of Electrical Engineering, Mechanical Engineering and Naval Architecture, R. 
Boškovića bb, HR-21000 Split, Croatia, E-mail: sikora@fesb.hr 
 
 
Summary: Different methods for simulation of sound propagation have different 
advantages and disadvantages regarding application in dispersive, non-homogenous 
media. There is not a single method that is able to fulfill all demands of such an 
environment. The methods that use principals of geometrical acoustics do not take 
refraction into account, and FEM can not be efficient enough in environments with 
complex 3D boundaries. 
In order to solve this problem a hybrid algorithm for beam-tracing method is designed. 
This algorithm is based on stochastical method, and is intended to solve the problem of the 
refraction, without sacrificing the accuracy and efficiency of beam-tracing method. 
 
Key words: ultrasound, sound, computer simulation 
 
 
1. INTRODUCTION 
 
There are several methods available today for simulation of sound propagation. Some are 
based on acoustic geometry models, such as ray-tracing and virtual source method. The other 
methods, such as FEM (finite element method) and BEM (boundary element method), divide 
the media in discrete elements and then by the means of numerical methods calculate the 
wave equation. Methods from both groups have their advantages and disadvantages, and this 
paper discusses the method of choice for simulation of sound propagation in complex 
dispersive environments. The need for such a simulation exists in case where in complex 3D 
geometries (such as human scull, or partially open stadium) the sound (or ultrasound) 
propagates in dispersive media and refraction occurs. 
The example of such environment is the human scull. The propagation of ultrasound inside 
the human scull occurs in brain - media which is not homogenous. Because dispersive nature 
of the media, the refraction of sound occurs during propagation. The other important 
characteristic of such an environment is that it has complex reflective boundaries. So when 
ultrasound that has been refracted during the propagation through media encounters the 
reflective boundary - scull, it is reflected either in specular or diffusive way. The other 
example of such environment is the propagation of sound in the open stadium. As in the first 
case, here the medium in which sound propagates has non-homogenous nature. The non-
homogenous acoustic properties of air can be caused by the temperature difference or by the 
atmospheric effects such as rain or wind. This causes the refraction of sound during the 
propagation. Also this environment has complex geometry of auditorium, and can possibly be 
partially covered by roof. All this boundaries cause reflections either specular or diffuse. 
The purpose of this paper is to investigate different algorithms in the perspective of usability 
in conditions mentioned above. In order to achieve and accurate and efficient simulation of 
the sound and ultrasound propagation in dispersive environments, an algorithm that would 
overcome the limitations of the present methods, is under development. 
 
 
2. PRESENT METHODS 
 
The present methods can be divided into two groups: the methods that use the principles of 
acoustical geometry (virtual sources, ray tracing…) and the methods that divide the 
environment into small, discrete parts used for numerical solving of the wave equation. 
The virtual source method is widely used for calculation of propagation of sound. The 
simulations that use this method first calculate the mirror image of the sound source, 
respecting the reflective boundary (Fig. 1). The reflection of the sound is then calculated as it 
virtually comes from the mirror image of the source. This algorithm can also be applied for 
the reflections of higher order. The biggest advantage of this method is its accuracy, because 
it uses the point detector, and not the spherical one (such as ray-tracing). The disadvantage is 
the fact that it doesn’t work fast enough for high-order reflections. Because of this the virtual 
source method is often used in simulations for the accurate calculation of early reflections.  It 
is used in several simulations that are available today ULYSES, CATT, HEAD 3D [1, 2, 3]. 
This method calculates only specular reflections, and needs a hybrid solution for calculation 
of diffuse reflections [4]. Also this method doesn’t take into account the refraction during the 
propagation of the sound in non-homogenous media. 
 
 
 
Fig. 1. Virtual source method; mirror images of source. 
 
The ray-tracing method is second common method for simulation of the sound propagation. It 
traces the propagation of number of rays from the sound source. The rays are reflected from 
boundaries, and the spherical detector is used in order to check if the rays reach receiver (Fig. 
2).  
The advantage of this method is that it is not time consuming even for high order reflections. 
This is due to fact that amount of computation doesn’t grow exponentially with the order of 
reflections, which is the case with virtual source method [3]. The disadvantage of this method 
is that it is not as accurate as virtual source method. This is due to fact that reception of ray is 
detected with spherical detector, and not the point one. So because of the size of detector there 
is chance of multiple ray detection, and also of missed ones. There are several simulations 
available that use this method: ULYSES, CATT, EASE [1, 2, 4]. These simulations overcome 
the problem of accuracy using the virtual source method for early reflections. Since the 
diffuse reflections are not implicitly solved by ray tracing, they are calculated by the means of 
hybrid methods. Although the ray-tracing is not generally used for calculation of the 
refraction of the sound, the UTSIM simulation shows that this method can be used for the 
calculation of refraction [5]. 
 
 
Fig. 2. Ray tracing method. 
 
The ray-tracing method has recently evolved into new forms such as cone-tracing, pyramid-
tracing and beam-tracing [6, 7]. This evolution was caused by the wish to improve the 
accuracy of the calculation, while maintaining the speed of the algorithm. In this simulation 
the source doesn’t emit the rays, instead it emits beams in form of cone (cone-tracing) or 
pyramid (pyramid-tracing and beam-tracing) (Fig. 3). In latter case the space around the 
source is divided into pyramids, and all geometrical calculations instead with the rays are 
done with pyramids. Since pyramids cover the complete space around the receiver, there is no 
need for spherical detector, so the point detector is used. This causes that there are no multiple 
reflections, and that no reflections are missed. Also the calculation of the energy of sound is 
more accurate, since the complete energy dissipated by the source can be accurately divided 
between pyramids [6]. After the pyramids that hit the receiver are detected their acoustic 
energy is summed in order to get the total amount of sound energy in the place of receiver. 
The method of beam-tracing [7] has the hybrid method similar to radiosity algorithm that 
takes into account the diffuse reflections. These methods don’t take into account the refraction 
of sound. 
 
 
Fig. 3. Cone tracing – left; pyramid tracing – right. 
 
The second group of simulations doesn’t use the principles of acoustical geometry for 
calculation of sound propagation. Instead they divide the environment in which the sound 
propagates into discrete parts (Fig. 4). On this segments are then used numerical methods for 
the solving of wave equations. By solving of the wave equations the propagation of sound is 
calculated both in means of geometry and the level of sound in the spot of the receiver. The 
example of such a method is a PZFLEX [8], an ultrasound simulator used for design of 
piezoelectric transducers. This simulation uses FEM method for the calculation of 
propagation of ultrasound. The main advantage of this method is that it is very accurate and 
takes into account all relevant wave phenomena such as refraction, reflection, diffraction and 
attenuation. The main disadvantage of this method is that it works efficiently only in 2D. In 
3D environment, and especially with complex boundaries, this method is not practical. 
 
 
Fig. 4. Discrete elements in FEM. 
 
 
3. ALGORITM FOR SIMULATION OF SOUND PROPAGATION IN COMPLEX 
DISPERSIVE ENFIRONMETS 
 
The FEM method has big advantages because it takes into account all relevant wave 
phenomena such. Because of this it is very accurate, and can be used in dispersive 
environments. But the limitation of the application of this method in complex 3D 
environments cannot be easily overcome. The main question is the increase in number of 
computations when discrete elements become 3D and not 2D elements. 
So some other method had to be found in order to simulate targeted environment. The natural 
candidate was the most advanced method from the first group of simulations. This was the 
beam-tracing method. This method can calculate propagation of sound in complex 3D 
environments with both speed and accuracy. Also it takes into account both the specular and 
diffusive reflections. So in order to deliver a quality simulation in dispersive environments, 
the only request is to solve the problem of the refraction in dispersive environment. 
This can be done by using a hybrid statistical method for calculation of sound propagation in 
dispersive, non-homogenous media. Such a complex media is simplified, and represented by 
two extreme media in terms of density. The beams that are propagating through such a media 
are divided into number of sections. These sections have density that is somewhere in 
between of two extreme media. The exact density is determined stochastically. Because of the 
different density, the refraction occurs, and the geometry of rays is calculated according to 
Snell’s law. Otherwise uniform propagation is disturbed, and this simulates the non-
homogenous nature of the media (Fig. 5.). 
 
 
 
Fig. 5. Beams in homogenous media – left; simulation of non-homogenous media – right. 
 
 
4. CONCLUSION 
 
Different methods for simulating the propagation of wave have different advantages and 
disadvantages regarding calculation of the propagation in dispersive, non-homogenous media. 
There is not a single method that is able to fulfill all demands of such an environment. The 
methods that use the laws of geometrical acoustics do not take refraction in dispersive media 
into account. On the other hand, FEM calculates refraction as well as other wave related 
phenomena, but sacrificing the efficiency in environments with complex 3D boundaries. 
In order to solve this problem, a hybrid algorithm for beam-tracing method is designed. For 
calculation of refraction in dispersive media this algorithm uses a stochastical method. The 
implementation of this algorithm is done taking care not to compromise the efficiency of 
classical beam-trace method.  
 
 
REFERENCES 
 
[1] 
ULYSSES Acoustic Design and Simulation Software, http://www.ifbsoft.de/ 
[2] 
Bengt-Inge Dalenbäck, The Rediscovery of Diffuse Reflection 
in Room Acoustics Prediction, ASA, Cancoun, 2002. 
[3] 
M. Sikora, Magistarski rad, Zagreb, 2000. 
[4] 
EASE Simulation Software, http://www.renkus-heinz.com/ 
[5] 
UTSIM Simulation Software, http://www.ndt.net/library/applic/mgarton/ut_sim.htm 
[6] 
A. Farina, RAMSETE - a new Pyramid Tracer for medium and large scale acoustic 
problems, Proc. of EURO-NOISE 95 Conference, Lyon, 1995 
[7] 
I. Drumm, The development and application of an adaptive beam tracing algorithm to 
predict the acoustics of auditoria, Ph.D. Thesis 
[8] 
G. Wojcik, J. Mould, L. Carcione, Combined Transducer And Nonlinear Tissue 
Propagation Simulations, 
1999 International Mechanical Engineering Congress & Exposition Proc. 
 
View publication stats
View publication stats
