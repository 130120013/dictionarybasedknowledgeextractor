ComputerSimulation Techniques for Acoustical
Designof Rooms
Jens Holger Rindel
The Acoustics Laboratory
Technical UnIversity of Denmark
DK-2800 Lyngby, Denmark
Abstract: After decades of developmentroom acoustical computer models have matured.
Hybrid methods
combine the best features from image source models and ray tracing methods and have lead to significantly
reducedcalculationtimes.
Due to the wavenatureof soundit has been necessaryto simulatescattering effectsin
the models.
Today'sroom acousticalcomputermodelshaveseveraladvantagescomparedto scale models.
They
have become reliable and efficient design tools for acousticconsultants, and the results ofa simulation can be
presentednot only for the eyesbut also for the ears withnewtechniques for auralisation.
INTRODUCTION
In acoustics as in many other areas of physics a basic question
is whether the phenomena should be described by particles or
by waves.
A wave model for sound propagation leads to more
or less efficient methods for solving the wave equation, like
the Finite Element Method (FEM) and the Boundary Element
Method (BEM).
Wave models are characterized by creating
very accurate results at single frequencies, in fact too accurate
to be useful in relation to architectural environments, where
results in octave bands are usually preferred.
Another problem
is that the number of natural modes in a room increases
approximately with the third power of the frequency, which
means
that for practical
use wave models
are typically
restricted
to low frequencies
and small rooms, so these
methods are not considered in the following.
Such a
geometrical model is well suited for sound at high frequencies
and
the
study
of interference with
large,
complicated
structures.
For the simulation of sound in large rooms there
are two classical
geometrical methods,
namely
the Ray
Tracing Method and the Image Source Method.
Forboth
methods it is a problem that the wavelength or the frequency
of the sound is not inherent in the model.
This means that the
geometrical models tend to create high order reflections which
are much more precise than would be possible with a real
sound wave.
So, the pure geometrical models should be
limited to relatively low order reflections and some kind of
statistical approach should be introduced in order to model
higher order reflections.
One way of introducing the wave
nature of sound into geometrical models is by assigning a
scattering
coefficient to each
surface.
In this way the
Acoustics Australia
reflection from a surface can be modified from a pure specular
behaviour into a more or less diffuse behaviour, which has
proven to be essential
for the development of computer
models that can create reliable results.
SIMULATION OF SOUND IN ROOMS
2.1 The Ray Tracing Method
The Ray Tracing Method uses a large number of particles,
which are emitted in various directions from a source point.
The particles are traced around the room losing energy at each
reflection
according
to the absorption coefficient
of the
surface.
In order to obtain a
calculation result related to a specific receiver position it is
necessary either to define an area or a volume around the
receiver in order to catch the particles when travelling by, or
the sound rays may be considered the axis ofawedge or
pyramid.
There is a reasonably high probability that a ray will
discover a surface.
with the area A after having travelled the
time t if the area of the wave front per ray is not larger than
A12.
According to this equation
a very large number of rays is necessary fora typical room.
As an example a surface area ofl0 m- and a propagation time
up to only 600 ms lead to around 100,000 rays as a minimum.
23 (1995) NO.3 - 81
The development of room acoustical ray tracing models
started some thirty years ago but the first models were mainly
meant to give plots for visual inspection of the distribution of
reflections [I].
The method was further developed [2],andin
order to calculate a point response the rays were transferred
into circular
cones
with
special
density
functions,
which
should
compensate for the overlap
between
neighbouring
cones [3].
However,itwasnotpossibletoobtainareasonable
accuracy with this technique.
Recently, ray tracing models
have been developed that use triangular pyramids instead of
circular cones [4], and this may be a way to overcome the
problem of overlapping cones.
2.2 The Image Source Method
The Image Source Method is based on the principle that a
specular reflection can
be
constructed geometrically by
mirroring the source in the plane of the reflecting surface.
In
a rectangular box shaped room it is very simple to construct
all image sources up to a certain order of reflection, and from
this it can be deduced that if the volume of the room is V,the
approximate number of image sources within a radius of ct is
N
ref=4;;3 (3
(2)
This is an estimate of the number of reflections that will arrive
at a receiver
up to the time
t after sound emission,
and
statisticallythise~uationholdsforanyroomgeometry.
The advantage of the image source method is that it is very
accurate, but if the room is not a simple rectangular box there
is a problem.
(3)
As an example we consider a 1,500 m3 room modelled by 30
surfaces.
From equation
(2) it
appears that less than 2500 of the 10 19imagesourcesarevalid
fora specific receiver.
For this reason image source models
are only used for simple rectangular rooms or in such cases
where low order reflections are sufficient, e.g.
2.3 The Hybrid Methods
The disadvantages of the two classical methods have led to
development of hybrid
models,
which
combine
the best
82 - Vol.
23 (1995) NO.3
features of both methods [7,8,9].
The idea is that an efficient
way to find image sources having high probabilities of being
valid is to trace rays from the source and note the surfaces they
hit.
Once 'backtracing' has found an image to be valid,
then the level of the corresponding reflection is simply the
product
of the energy
reflection coefficients of the walls
involved and the level of the source in the relevant direction of
radiation.
It is, of course, common for more than one ray to follow
the same
sequence of
surfaces, and
discover the
same
potentially valid images.
It is necessary to ensure that each
valid
image
is only
accepted once,
otherwise duplicate
reflections would appear in the reflectogram and cause errors.
Therefore it is necessary to keep track of the early reflection
images found, by building an 'image tree'.
Thereafter, some
other method has to be used to generate a reverberation tail.
This part of the task is the focus of much effort, and numerous
approaches have been suggested, usually based on statistical
properties of the
room's
geometry and
absorption.
One
method, which has proven to be efficient, is the 'secondary
source'
method
used
in the ODEON program [9].
This
method is outlined in the following.
Each secondary source is considered to radiate into
a hemisphere as an elemental area radiator.
Thus the late reflections are
specific to a certain receiver position and it is possible totake
shielding and convex room shapes into account.
hybrid modeLT""l oound!a)'l CTUte
imo,e IOWUJ ror c..ty rd tioru IfId _onduy IOILR:.. 011
1hc:u11,forlllerdlcctiOll'-
Figure 1 illustratts in tchematic fonn how the u.kulsliOl'l
model beh..u In the figure.
more oornpliC1ttd room
!hilmiShlnolbctruefor.l1 imagelOU1CC$.Thc:oonllibutions
from5, 51 and 51l arrive .1 he KUivcr at limes propoctional
10 !heir distanen fromthe rcceivn.
In the simple boI-wped room thne arc:
.11 visible
from
Ihc
receiv er, .nd thus
!hey .
Also shown is
the
reverse-inte grated decay curve,
whic h i,
used for
c. lculalion ofl'Cllerocralion time and other room acooslical
parameters.
Since the early reflmions are delCTlnined more
KCUralelylhanthc la( retkctiOll$oncmigblthinkwlbettn'
1Ei~'~ T_
SS,
S b
c d e f l
b
Fe- 2 ketloaoco- b'''' ~ P.ill F'Fft I.
Fi, ure 3. tYPicl-limpulse n sporlse(energy) and decl-~ Cu.....
calcutltedwilh lhybri dmodol
results arc obt.J.incd with the transition
order II
hi~h
I-~
possible.
for I- givcn nurnber of rays the clwlc r of
miw"i some iml.gcs incruses with reflection order and ....ith
the nwn bcr of VIl.11s..n~s in the room.
in real life, rcnC'Clionl
from
slMII
SUrfKU I-re generally mucb ""C.ker
' hI-fi
cal~l.ttd by the ..... , of gcornctrical KOUItics, 50 any such
reflection s miurd by the mode l
1fT in reI-lily of less
sipirlQ/tCCtlwt the model ibcl f_1d SIIUest .
the
efforts of an extended wo:u ll-tion mlly Iud to worw results
Recent upcriments ",i!h
Ihe ODEOS PfOlra m hI_e
shcrwn thaI only Soo to 1000
ra~s Ill: sufficient 10 obtain
nd il-b1e n:iuh s in a !
This me;JllS
thai I-hybrid model like this can give much bcnc r resultl than
either of ute pure basic melhods.
proportio nal 10the projection of the wall area.
3
Figure 4,One sound ray in 1 simple room wilh differenl V1I]UC'S
o( thc .
urfacesc.llncriog coclficicnt
An example of ray tracing with different valucs of the
scanering coefficient is shown in Fig.
The
room is a
rcclll1gular box with a relativelylow ceiling.
1 for large,
planc surfaces and 10around 0.7 for highly irregu lar surfaces.
Scaneriog eoe fflcients as low as 0.02 have been found in
studiesofa reverberation chamberwithout diffusing clements.
In principle thc scatteri ng coe ff icient varies with
the frcqueneyirscattering due to the finite siu of a surface i.
mosl pronounc ed at low frequencies , whereas scattering due
10 irregu larities of the surface occurs al bigh frequencies.
uffieient to eharact crize each surface by only one
scan ering cce fflcie m, valid for all frequ encies.
4, ACCURACYMi D CALCULATIO~ TIM E
Rcuntly an international round rob in has bee n carried out
[II) with 16parneipants, most of them developers of software
for room acoustical simulations.
It came out that
onlythrec programs can be assumed to give unquestion ably
reliable results, The results of these program s differ from the
average measurement results by the same order of magnitude
Acous tics AI,I$I,/Ilia
as the individual measurement results.
It is
interesting to note, that the three best programs (one of which
is the ODEON program) use some kind of diffuse reflections,
whereas the results from purely specular models were more
outlying.
It is also typical that the best programs do neither
require extremely long calculation
times nor extremely
detailed room geometries.
ADVANTAGESOF COMPUTER MODELS
COMPARED TO SCALE MODELS
It is quite obvious that a computer model is much more
flexible thana scale model.
It is easy to modify the geometry
of a computer model, and the surface materials can be
changed just by changing the absorption coefficients.
The
computer model is fast, typically a new set of results are
available a few hours after some changes to the model have
been proposed.
The most important advantage is probably that the
results can be visualised and analysed much better because a
computer model contains more information than a set of
measurements done in a scale model with small microphones.
Although it is difficult to extract specific
results from such a spatial analysis, it can help to understand
how a room responds to sound.
5.3 Grid Response Displays
With a computer model it is straight forward to calculate the
response at a large number of receivers distributed in a grid
that covers the audience area.
5.4 Auralisation
In principle it is possible to use impulse responses measured
ina scale model forauralisation.
Thetransducers
are one reason that the computer model is superior for
auralisation.Anotherreasonisthattheinformationabouteach
reflection's direction of arrival allows a more sophisticated
modelling of the listener's head-related transfer function.
Most of the recent research conceming auralisation has
concentrated on the 'correct' approach,wherebyeachlinkin
the chain from source to receiver may be modelled as an
impulse response.
[13] for an overview of
the technique.
However, the convolution technique involved
requires either expensive hardware for real-time convolution
or Jongwaits for off-line convolution, and often the impulse
response is too short to produce a realistic reverberation.
An
alternative technique for auralisation,
which avoids the
convolution bottleneck, has recently been proposed [14].
The
method is based on an interface between a digital audio
mainframe and a room acoustical computer model.
This
means that auralisation can follow immediately after the room
acoustical calculation in a receiving point, and there are no
limitations on length of the source signal.
The early
reflections and the late reverberant reflections are treated by
two different techniques.
With this technique 40-50 early
reflections will usually be sufficient to create a realistic
sounding room simulation, and long reverberation time is no
problem.
For presentation through headphones
the following three methods are used in order to obtain
localization outside the head:
 Interaural time difference.
The spectral peaks and notches due to the
outer ear arc roughly simulated by filters Although this is
known to be the main cue for elevation, the intention at
this stage has not been to create a localization fordifTerent
elevation angles, but rather to avoid the front-back
confusion
and
to
improve
the
out-of-the-head
localization.
Several acoustical
problems in a room can easily be detected with the ears,
whereas they may be difficult to express with a parameter that
can be calculated.
CONCLUSION
Computer techniques for simulation of sound in rooms have
improved significantly in recent years, and for the consultant
the computer model offers several advantages compared to the
scale model.
The scattering of sound from surfaces has
appeared to be very important in room acoustical simulation
technique,andthishascreatedaneedforbetterinformation
about the scattering properties of materials and structures.
Although the scattering can be handled by the model, the
knowledge about which scatteringcoefTicients to use is very
Vol.
So, it can be concluded that there
is a need
for a
method to measure the
scattering coefficient of surfaces.
Until then there remains an inherent piece of guesswork in
room acoustical simulations.
A. Krokstad,
S. Stroem,
and
S. Soersdal,
"Calculating the
Acoustical
Room
Response
by the
use of a Ray Tracing
Technique"J.SoundVib.8,118-125(1968).
A. Kulowski, "Algorithmic Representation of the Ray Tracing
Technique" Applied Acoustics 18, 449-469 (1985).
Vian,and D. van Maercke, "Calculation of the Room Impulse
Response using a Ray-Tracing Method" Proc.
T. Lewers, "A Combined Beam Tracing and Radiant Exchange
Computer Model of Room Acoustics" Applied Acoustics 38,
161-178(1993).
Berkley, "Image method for efficiently
simulatingsmall-roomacoustics"J Acoust.
M. vorlander,
"Simulation of the transient
and steady-state
sound
propagation in rooms
using
a new combined ray-
tracing/image-source algorithm" J Acoust.
8. a.M.
Naylor, "ODEON
- Another
Hybrid Room Acoustical
Modei"AppliedAcoustics38,13l-l43(l993).
9. a.M.
Naylor, "Treatment of Early and Late Reflections in a
Hybrid Computer
Model
for Room
Acoustics" 124th ASA
Meeting, New Orleans (1992) Paper 3aAA2.
M. Vorlander,"International Round Robin on Room Acoustical
Computer Simulations" Proc.
ISOIDIS 3382 "Measurement of the reverberation time of rooms
with reference to other aeoustical parameters" (1995).
Rindel, C. Lynge, G. Naylor and K. Rishoej, "The Use of a
Digital Audio Mainframe
for Room Acoustical
Auralization,"
96th Convention of the Audio Engineering Society, Amsterdam
(1994),AESPreprint3860
TONTiNE POLYESTER
ACOUSTIC INSULATION
 Tontine Sound Batts
 Tontine Black Liner
 Tontine Foiled & Faced Polyester Pads
Suitable for specialised acoustic insulation suchas
recording studios,cinemas, entertainment centres,
auditoriums, air-conditioning duct,wallinfill
andceilingoverlays.
