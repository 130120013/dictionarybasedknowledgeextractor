See discussions, stats, and author profiles for this publication at: https://www.researchgate.net/publication/324110973
Acoustics Treatment for University Halls
Chapter  January 2018
DOI: 10.1007/978-3-319-64349-6_18
CITATIONS
0
READS
86
3 authors, including:
Abu Hurrara Ali
University of Engineering and Technology, Lahore
12 PUBLICATIONS118 CITATIONS
SEE PROFILE
Emad Mushtaha
University of Sharjah
89 PUBLICATIONS471 CITATIONS
SEE PROFILE
All content following this page was uploaded by Emad Mushtaha on 26 November 2021.
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
 
 
ACOUSTICS TREATMENT FOR UNIVERSITY HALLS 
Mahmood Abu Ali , Amar Abdulkareem, Ali Kareem, Dr. Emad Mushtaha 
 
University of Sharjah, college of Engineering, Department of Architectural Engineering, Sharjah, U.A.E 
 
 
 
Abstract 
 
This paper aims to solve one important problem while 
designing halls with domes in university such as a central 
lobby of Architectural Engineering Department at the 
University 
of 
Sharjah.
The absence of sound quality and clarity of speech in the 
central hall of AE Department has led to non-comfortable 
sound environment for conducting some activities such as 
juries and exhibitions.
Listening to a person giving his/her 
presentation or talk could be difficult  due to the large 
volume of the space, finishing materials inside the building, 
and the effect of the dome that causes an excess amount 
of reverberation time (RT).
This paper discusses various 
factors that affect sound environment such as building 
shape, 
volume, 
and 
materials.
Using 
REVIT 
and 
ECOTECT model in the simulation, the study could 
suggest alternatives to the existing building scenario.
Furthermore, an ideal combination of various techniques 
has been proposed to achieve economical and acoustical 
aspects.
Keywords: University halls, Acoustics, Reverberation 
Time, ECOTECT.
Reverberation time is the single most important parameter 
for judging the acoustical properties of a room and its 
suitability for various uses (Lamancusa, 2000).
The 
Architectural Engineering building in the University of 
Sharjah is built square with a semi-spherical dome placed 
on top of a central atrium, in which many activities are 
conducted.
The low absorption characteristics of 
marble and plaster foster the effect of creating a highly 
reverberant space that is unsuitable for activities such as 
juries,  or exhibitions, food events, etc.
Methodology 
 
In order to explore evaluate the sonic environment in AE 
Dept., 
the 
study 
started 
by 
investigating 
sound 
environment in the building using sound meter for two 
different days.
Decibels were recorded for the targeted 
area as shown in figure 1 & 2.
Sound Meter Measuring in five different locations 
in the central lobby 
 
Sound meter was used to measure sound levels with 
HVAC System and mechanical equipment in the building 
for five different locations inside the central lobby.
The readings were as follows: 
 
Point 1: 50.2 dB  
Point 2: 46.5 dB 
Point 3: 44.6 dB 
Point 4: 45.6 dB  
Point 5: 46.8 dB   
 
Another experiment was taken during weekdays for ground  
And first floors when it is occupied.
Results in figure2 demonstrate 
sound levels received during working days/hours (13:00 - 
16:00).
Sound Levels 
Floor time 
10:30 am 
12:30 pm 
2:30 pm 
(Peak Hour) 
4:30 pm 
Ground 
60.8 dB 
57.7 dB 
68.3 dB 
59.5 dB 
First 
66.3 dB 
67.6 dB 
74.6 dB 
50.2 dB 
1 
4
 1  
3 
2 
5 
Abu Ali, M., Abdulkareem, A., Kareem, A., Mushtaha, E. 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
 
2.2.
Factors Affecting the Acoustical Behavior in 
University Halls 
 
According to Sabine's equation in calculating the 
reverberation time, there are two important factors that 
alter the acoustical behavior in any building (Szokolay, 
2003).
The main factor is the volume, so reducing the 
volume of the department will efficiently lower the 
reverberation time and consequently decrease sound 
levels in that area.
As to be clarified in the upcoming 
discussions, a method was sought to reduce the volume 
without changing the shape of the building or dome.
The 
second factor is related to finishing materials applied on 
building surfaces.
+
3
T60  = K [0.0118  V

(1) 
 
With a volume of 3055.7 m3 and K=4 for speech, then: 
 
0.1070] 1.11s                                  
 +
3
[0.0118   3055.7
 4 = 
T60


 
With Sabine formula,  
 
A                                
T60  = 0.161 V






                          (2) 
 
The existing RT value could be obtained.
Taken the 
condition that there are no seats or people within the 
space: 
 
49.03    10.03s                             
T60  = 0.161 3055.7
 





  
 
A reasonable explanation for such a large RT value is the 
extremely low absorption coefficients of existing finishing 
materials.
Because of this, the total absorption area was 
found to be 49.03 m2 Sabines.
Computer Model Simulation 
 
 
 
Figure 2.
Revit Model for M8 Building 
 
Given the nature and complexity of the interior geometry of 
the university hall examined, implementing a feasible 
computer model theory is important to obtain good quality 
data.
In this work, a REVIT model was exported to 
ECOTECT to obtain the exact surface areas and volume 
taking into consideration the pervious calculations.
After 
Simulating the existing case, different kinds of actions 
were applied to the model.
Thus, the 
university halls are generally poorly suited for delivering 
speeches because of the excess in reverberation time.
(Hz- ms) graph & estimated decay 
 
Abu Ali, M., Abdulkareem, A., Kareem, A., Mushtaha, E. 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
6.
Acoustical treatments 
 
As outlined previously, the central hall requires more 
absorption surfaces to include in the building in order to 
dramatically enhance the acoustical environment.
Given this 
effect, the reflected sound energy from the dome returns to 
the main space with a time delay, resulting in echoes or 
noise in the hall and the reduction in the percentage of 
intelligibility.
Reducing the volume by adding mesh 
between the ground floor and the first floor: 
 
Mesh fabric was inserted in the model between ground 
and first floor to improve the acoustical environment.
(Hz-ms) & estimated decay graphs after applying 
mesh fabric 
 
6.2 Adding absorption panels 
 
As indicated earlier, more absorption was required for the 
mid-frequency range shown in Figure 5.
This result is almost similar to past 
trial, which is a good alternative.
Adding carpet 
 
In general, carpets have a high sound absorption that 
reaches up to 50% when used over large areas.
The proposal has performed 
better than earlier options of absorption panels and 
reduction of volumes.
(Hz-ms) & estimated decay graphs after adding 
carpet 
Abu Ali, M., Abdulkareem, A., Kareem, A., Mushtaha, E. 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
6.4.
The authors have integrated this wood 
classing on internal walls of the both ground and first floors 
within the central hall area.
From the simulation, the RT 
was decreased to 2.76 ms at 500 Hz ( Figure 7) 
 
 
 
 
 
Figure 7.
(Hz-ms) & estimated decay graphs after adding 
carpet 
 
6.5 Adding all methods at once 
 
Although different alternatives were applied individually to 
enhance the acoustical behavior and reduce the RT 
levels,, combining them together will probably magnify 
their effect and therefore achieve better satisfactory 
results.
This combination has reached to an acceptable RT 
reduction value of approximate 0.7 ms from 10 ms, which 
is about 90% improvement (Figure 8) 
 
 
 
 
 
Figure 8.
(Hz-ms) & estimated decay graphs after applying 
all methods 
 
7.
Cost of Applied Materials 
 
Earlier simulation showed that adding all methods at once 
was certainly the most efficient technique to have a 
maximum reduction of the reverberation time.
Thus, it is very important to roughly estimate the 
cost of each proposal.
Therefore, a strategy that satisfies 
the objective in terms of cost and absorption would be as 
shown below: 
 
1- Mesh Fabric cost = Area x Cost (per square meter) = 
141.7 m2 x $2 = $243.4 
2- Absorption panels cost = Area x Cost (per square 
meter) = 88.9 m2 x $80.8 = $7,183.1 
3- Carpet cost = Area x Cost (per square meter) = 
 405.7 m2 x $5.3 = $2,139 
4- Wood cladding cost = Area x Cost (per square meter) = 
456 m2 x $32 = $14,592 
5- All methods cost = $24,157.5 
 
8.
Conclusion 
 
In this work, the acoustic environment of common Sharjah 
university halls was evaluated.
These low absorption 
coefficients of materials in halls resulted in a reduction of 
the total absorption area of the space.
The computer 
simulation for the hall revealed that the materials 
commonly used for the interior needed acoustic treatment.
This treatment will establish more absorption to the space 
and reduce any unfavorable acoustic occurrence that 
might happen from having surfaces that reflect large 
amounts of acoustic energy within the space.
The installation of porous panels and the 
addition of absorption by changing the different surface 
materials characteristics improved acoustic quality, 
indicating that these techniques are effective.
A decrease 
in RT was observed.
Taking acoustic absorption into account by 
applying all the methods will ultimately produce the 
maximum sound quality.
However, 
Using mesh and carpet, or absorption panels and carpet 
Abu Ali, M., Abdulkareem, A., Kareem, A., Mushtaha, E. 
3rd International Sustainable Buildings Symposium (ISBS 2017), 15-17 March 2017, Dubai 
 
together, could satisfy both of these aspects in the most 
effective and economic approach.
Environmental science in 
building.
